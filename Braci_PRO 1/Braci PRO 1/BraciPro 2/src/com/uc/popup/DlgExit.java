package com.uc.popup;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.uc.prjcmn.PRJFUNC;
import com.ugs.braci.R;

public class DlgExit extends Activity implements OnClickListener {

	public static final String RESULT_STR = "SELECT_ITEM";

	public static final int SELECT_OPT_LOGOUT = 1;
	public static final int SELECT_OPT_SWITCHOFF = 2;
	public static final int SELECT_OPT_RUNINBACK = 3;

	private int m_nOption = SELECT_OPT_LOGOUT;

	private ImageView m_ivLogout;
	private ImageView m_ivSwitchOff;
	private ImageView m_ivRunInBack;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		getWindow().setFlags(WindowManager.LayoutParams.ALPHA_CHANGED,
				WindowManager.LayoutParams.ALPHA_CHANGED);
		getWindow().setGravity(Gravity.CENTER);

		setContentView(R.layout.dlg_exit);

		updateLCD();
		
		updateUIOptions();
	}

	@Override
	public void onBackPressed() {
		onCancel();

		super.onBackPressed();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.frm_logout:
			m_nOption = SELECT_OPT_LOGOUT;
			updateUIOptions();
			break;
		case R.id.frm_switch_off:
			m_nOption = SELECT_OPT_SWITCHOFF;
			updateUIOptions();
			break;
		case R.id.frm_runinback:
			m_nOption = SELECT_OPT_RUNINBACK;
			updateUIOptions();
			break;
		case R.id.tv_ok:
			onSelect();
			break;
		}
	}

	private void updateUIOptions() {
		m_ivLogout.setSelected(m_nOption == SELECT_OPT_LOGOUT);
		m_ivSwitchOff.setSelected(m_nOption == SELECT_OPT_SWITCHOFF);
		m_ivRunInBack.setSelected(m_nOption == SELECT_OPT_RUNINBACK);
	}

	/**
	 * 
	 */
	private void updateLCD() {
		m_ivLogout = (ImageView) findViewById(R.id.iv_opt_logout);
		m_ivSwitchOff = (ImageView) findViewById(R.id.iv_opt_switchoff);
		m_ivRunInBack = (ImageView) findViewById(R.id.iv_opt_runinback);

		View frm = findViewById(R.id.frm_logout);
		frm.setOnClickListener(this);

		frm = findViewById(R.id.frm_switch_off);
		frm.setOnClickListener(this);

		frm = findViewById(R.id.frm_runinback);
		frm.setOnClickListener(this);
		
		TextView _tv = (TextView)findViewById(R.id.tv_ok);
		_tv.setOnClickListener(this);
	}

	// ///////////////////////////////////////////
	private void onSelect() {
		Intent intent;
		intent = getIntent();
		intent.putExtra(RESULT_STR, m_nOption);
		setResult(RESULT_OK, intent);
		finish();
	}
	
	private void onCancel() {
		setResult(RESULT_CANCELED);
		finish();
	}
}
