package com.uc.prjcmn;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.UUID;

import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Location;
import android.provider.Settings.Secure;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.TextView;

import com.getpebble.android.kit.PebbleKit;
import com.getpebble.android.kit.util.PebbleDictionary;
import com.parse.ParsePush;
import com.uc.popup.DlgYesNo;
import com.uc.sqlite.SqlUtilities;
import com.ugs.braci.GlobalValues;
import com.ugs.braci.HomeActivity;
import com.ugs.braci.R;
import com.ugs.braci.engine.DetectingData;
import com.ugs.braci.engine.SoundEngine;
import com.ugs.lib.GPSTracker;
import com.ugs.lib.LayoutLib;

public class PRJFUNC {
	public static String TAG = "_PRJFUNC";
	public static boolean DEFAULT_SCREEN = true;

	public static int DETECTED_NUMBER = 1;
	public static int DETECTED_TYPE = 1;

	public static LayoutLib mGrp = null;

	public static int W_LCD = 0;
	public static int H_LCD = 0;
	public static int H_STATUSBAR = 0;
	public static int DPI = 0; // density dpi

	public static String DETECT_TEMP_PATH = "TempData";

	public static GPSTracker gpsTracker;

	private static Typeface mFace_FONT_OpenSans = null;
	private static Typeface mFace_FONT_OpenSansBold = null;

	public static HomeActivity g_homeActivity = null;

	public static String g_strDeviceID = null;

	public static String g_strOldMatchingPath = null;

	public enum Signal {
		NONE, FIRE_ALARM, DOOR_BELL, BABY_CRYING, MOBILE_RINGING, CAR_HORN, THIEF_ALARM, ALARM_CLOCK, WAKE_UP, BED_TIME, SOS, MEAL_TIME, NO, YES, CALLING_YOU, TRAFFIC_TONE, TRAIN_WHISTLE, MICROWAVE, POLICE_SIREN, INTERCOM, DOG_PARK, SNORE, SMOKE_ALARM, CO2, BACKDOOR_BELL, STOP,
	}

	public static final String[] SIGNAL_NAMES = { "Fire", "Doorbell",
			"BabyCrying", "Mobile", "CarHorns", "Thief", "AlarmClock",
			"Wakeup", "BedTime", "SOS", "MealTime", "No", "Yes", "Calling",
			"TrafficTone", "TrainWhistle", "Microwave", "PoliceSiren",
			"Intercom", "DogPark", "Snore", "SmokeAlarm", "CO2", "BackDoorbell" };

	public static final String[] STR_DEAF_PATHS = { "Bellman", "Byron/Byron",
			"Byron/ByronPlugin", "Friedland/FriedlandLibra",
			"Friedland/FriedlandEvo", "Friedland/FriedlandPlugin",
			"GreenBrook/GreenBrook", "Echo/Echo", "Geemark/GeemarkCL1",
			"Geemark/GeemarkCL2", "Geemark/GeemarkAmplicall",
			"Amplicom/Amplicom", };

	public static final String[] STR_DEAF_SOUND_TYPES = { "Doorbell", "BackDoorbell", 
			"SmokeAlarm", "Telephone", "Intercom", "CO2", "AlarmClock",
			"TheifAlarm", "MicroWave" };

	public enum GET_DATA_MODE {
		FROM_ASSETS, FROM_DB,
	}

	public static final GET_DATA_MODE _getDataMode = GET_DATA_MODE.FROM_ASSETS;

	public static int m_curRecordingSoundType = 0;
	public static int m_curRecordingDoorbellType = 0;

	public static String getDeviceID(Context pContext) {
		if (PRJFUNC.g_strDeviceID == null)
			PRJFUNC.g_strDeviceID = Secure.getString(
					pContext.getContentResolver(), Secure.ANDROID_ID);
		return PRJFUNC.g_strDeviceID;
	}

	/**
	 * reset graph value - mGrp
	 * 
	 * @param context
	 */
	public static void resetGraphValue(Context context) {

		if (mGrp != null) {
			mGrp = null;
		}

		DisplayMetrics displayMetrics = context.getResources()
				.getDisplayMetrics();
		SharedPreferencesMgr pPhoneDb = new SharedPreferencesMgr(context);
		pPhoneDb.loadScreenInfo();
		PRJFUNC.mGrp = new LayoutLib(PRJCONST.SCREEN_DPI, PRJFUNC.DPI, RS.X_Z,
				RS.Y_Z, displayMetrics.density);
	}

	public static Typeface getFont(Context context, String font) {
		Typeface typeFont = null;
		if (font.equals(PRJCONST.FONT_OpenSansBold)) {
			if (mFace_FONT_OpenSansBold == null) {
				mFace_FONT_OpenSansBold = Typeface.createFromAsset(
						context.getAssets(), font);
			}
			typeFont = mFace_FONT_OpenSansBold;
		} else if (font.equals(PRJCONST.FONT_OpenSans)) {
			if (mFace_FONT_OpenSans == null) {
				mFace_FONT_OpenSans = Typeface.createFromAsset(
						context.getAssets(), font);
			}
			typeFont = mFace_FONT_OpenSans;
		}
		return typeFont;
	}

	public static void setTextViewFont(Context context, TextView tv, String font) {
		if (tv == null || context == null) {
			return;
		}
		// if (mGrp == null)
		// resetGraphValue(context);

		Typeface typeFont = getFont(context, font);
		if (typeFont != null)
			tv.setTypeface(typeFont);
	}

	public static void setTextViewFont(Context context, CheckBox chkBox,
			String font) {
		if (chkBox == null || context == null) {
			return;
		}
		// if (mGrp == null)
		// resetGraphValue(context);

		Typeface typeFont = getFont(context, font);
		if (typeFont != null)
			chkBox.setTypeface(typeFont);
	}

	public static int ConvSignalToInt(Signal p_signal) {
		int ret = -1;
		if (p_signal == Signal.FIRE_ALARM) {
			ret = 0;
		} else if (p_signal == Signal.DOOR_BELL) {
			ret = 1;
		} else if (p_signal == Signal.BABY_CRYING) {
			ret = 2;
		} else if (p_signal == Signal.MOBILE_RINGING) {
			ret = 3;
		} else if (p_signal == Signal.CAR_HORN) {
			ret = 4;
		} else if (p_signal == Signal.THIEF_ALARM) {
			ret = 5;
		} else if (p_signal == Signal.ALARM_CLOCK) {
			ret = 6;
		} else if (p_signal == Signal.WAKE_UP) {
			ret = 7;
		} else if (p_signal == Signal.BED_TIME) {
			ret = 8;
		} else if (p_signal == Signal.SOS) {
			ret = 9;
		} else if (p_signal == Signal.MEAL_TIME) {
			ret = 10;
		} else if (p_signal == Signal.NO) {
			ret = 11;
		} else if (p_signal == Signal.YES) {
			ret = 12;
		} else if (p_signal == Signal.CALLING_YOU) {
			ret = 13;
		} else if (p_signal == Signal.TRAFFIC_TONE) {
			ret = 14;
		} else if (p_signal == Signal.TRAIN_WHISTLE) {
			ret = 15;
		} else if (p_signal == Signal.MICROWAVE) {
			ret = 16;
		} else if (p_signal == Signal.POLICE_SIREN) {
			ret = 17;
		} else if (p_signal == Signal.INTERCOM) {
			ret = 18;
		} else if (p_signal == Signal.DOG_PARK) {
			ret = 19;
		} else if (p_signal == Signal.SNORE) {
			ret = 20;
		} else if (p_signal == Signal.SMOKE_ALARM) {
			ret = 21;
		} else if (p_signal == Signal.CO2) {
			ret = 22;
		} else if (p_signal == Signal.BACKDOOR_BELL) {
			ret = 23;
		} else if (p_signal == Signal.STOP) {
			ret = 101;
		}
		return ret;
	}

	public static Signal ConvIntToSignal(int p_number) {
		Signal ret = Signal.NONE;
		if (p_number == 0) {
			ret = Signal.FIRE_ALARM;
		} else if (p_number == 1) {
			ret = Signal.DOOR_BELL;
		} else if (p_number == 2) {
			ret = Signal.BABY_CRYING;
		} else if (p_number == 3) {
			ret = Signal.MOBILE_RINGING;
		} else if (p_number == 4) {
			ret = Signal.CAR_HORN;
		} else if (p_number == 5) {
			ret = Signal.THIEF_ALARM;
		} else if (p_number == 6) {
			ret = Signal.ALARM_CLOCK;
		} else if (p_number == 7) {
			ret = Signal.WAKE_UP;
		} else if (p_number == 8) {
			ret = Signal.BED_TIME;
		} else if (p_number == 9) {
			ret = Signal.SOS;
		} else if (p_number == 10) {
			ret = Signal.MEAL_TIME;
		} else if (p_number == 11) {
			ret = Signal.NO;
		} else if (p_number == 12) {
			ret = Signal.YES;
		} else if (p_number == 13) {
			ret = Signal.CALLING_YOU;
		} else if (p_number == 14) {
			ret = Signal.TRAFFIC_TONE;
		} else if (p_number == 15) {
			ret = Signal.TRAIN_WHISTLE;
		} else if (p_number == 16) {
			ret = Signal.MICROWAVE;
		} else if (p_number == 17) {
			ret = Signal.POLICE_SIREN;
		} else if (p_number == 18) {
			ret = Signal.INTERCOM;
		} else if (p_number == 19) {
			ret = Signal.DOG_PARK;
		} else if (p_number == 20) {
			ret = Signal.SNORE;
		} else if (p_number == 21) {
			ret = Signal.SMOKE_ALARM;
		} else if (p_number == 22) {
			ret = Signal.CO2;
		} else if (p_number == 23) {
			ret = Signal.BACKDOOR_BELL;
		} else if (p_number == 101) {
			ret = Signal.STOP;
		}
		return ret;
	}

	public static void testPebbleConnected(Context context) {
		GlobalValues.m_bPebbleWatch = PebbleKit.isWatchConnected(context);
	}

	public static final UUID[] NOTIFY_UUIDs = { UUID
			.fromString("69721c7e-69ef-40eb-a687-5649124ab141") };

	public static void closeAppOnPebble(Context context) {
		if (NOTIFY_UUIDs != null) {
			for (int i = 0; i < NOTIFY_UUIDs.length; i++) {
				PebbleKit.closeAppOnPebble(context, NOTIFY_UUIDs[i]);
			}
		}
	}

	public static void deleteFile(File file, boolean bExcpetDir) {
		if (file.isDirectory()) {
			String[] miniChildren = file.list();
			for (int j = 0; j < miniChildren.length; j++) {
				File child = new File(file, miniChildren[j]);
				deleteFile(child, false);
			}
		}

		if (!bExcpetDir)
			file.delete();
	}

	public static void sendPushMessage(Context context, Signal signal) {
		// Push Notification
		if (!PRJCONST.IsInternetVersion)
			return;

		JSONObject data1 = null;
		try {
			String strMessage = PRJFUNC.getDeviceID(context) + ","
					+ ConvSignalToInt(signal) + "," + PRJFUNC.DETECTED_NUMBER;
			data1 = new JSONObject(
					"{\"action\": \"android.intent.action.PUSH_STATE\", \"message\": \""
							+ strMessage + "\", \"alert\": \"" + signal
							+ "- From " + GlobalValues.m_stUserName + "\"}");

			ParsePush push = new ParsePush();
			push.setChannel(PRJFUNC
					.convertEmailToChannelStr(GlobalValues.m_stUserName));
			// MainActivity.push.setMessage("The Giants just scored! It's now 2-2 against the Mets.");
			// MainActivity.push.setMessage(alarm);
			push.setData(data1);
			push.sendInBackground();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void sendSignalToPebble(Context context, Signal signal) {
		if (!GlobalValues.m_bNotifyPebbleWatch
				&& !GlobalValues.m_bNotifyAndroidWear)
			return;

		if (Signal.NONE != signal) {
			Log.d(TAG, "onSignalDetected: " + signal);
			for (int times = 0; times < NOTIFY_UUIDs.length; times++) {
				if (signal != Signal.STOP) {
					PebbleKit.startAppOnPebble(context, NOTIFY_UUIDs[times]);
					try {
						Thread.sleep(1800);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}

				if (signal == Signal.INTERCOM || signal == Signal.BACKDOOR_BELL) {
					signal = Signal.DOOR_BELL;
				}

				if (signal == Signal.SMOKE_ALARM || signal == Signal.CO2) {
					signal = Signal.FIRE_ALARM;
				}

				int buttonID = ConvSignalToInt(signal);
				if (buttonID < 0)
					return;

				PebbleDictionary data = new PebbleDictionary();
				data.addUint8(0, (byte) buttonID);
				PebbleKit.sendDataToPebble(context.getApplicationContext(),
						NOTIFY_UUIDs[times], data);
			}
		}
	}

	// public static void putAssetsToDb(Context pContext) {
	// String strRootDetectDir = "detect/Main";
	// if (PRJCONST.IsSecondVersion)
	// strRootDetectDir = "detect/Second";
	//
	// SqlUtilities sqlUtilities = new SqlUtilities(pContext);
	//
	// sqlUtilities.initTable();
	//
	// for (int i = 0; i < SIGNAL_NAMES.length; i++) {
	// String strDir = strRootDetectDir + "/" + SIGNAL_NAMES[i];
	// for (int j = 0; j < PRJCONST.MAX_FIRE_SOUND_CNT; j++) {
	// String strFile = strDir + "/Detect" + j + ".dat";
	// long ret = sqlUtilities.insertData(SIGNAL_NAMES[i], strFile);
	// if (ret < 0)
	// break;
	// }
	// }
	//
	// sqlUtilities.close();
	// }

	public static void releaseDetectDataFromDb(Context pContext) {
		try {
			SqlUtilities sqlUtilities = new SqlUtilities(pContext);
			for (int i = 0; i <= SIGNAL_NAMES.length; i++) {
				sqlUtilities.getDetectData(ConvIntToSignal(i));
			}

			sqlUtilities.close();
		} catch (Exception e) {
		}
	}

	public static void loadDetectDataToMatch(Context pContext,
			String strDirPath, int nMaxFramesCnt) {

		Log.i("BraciPro", "Load data to match " + strDirPath);
		if (g_strOldMatchingPath != strDirPath)
			GlobalValues._matchingData = null;

		if (GlobalValues._matchingData != null)
			return;

		GlobalValues._matchingData = new ArrayList<DetectingData>();
		for (int i = 0; i < PRJCONST.MAX_FIRE_SOUND_CNT; i++) {
			DetectingData detectData = new DetectingData();
			detectData.LoadDetectData(strDirPath,
					String.format("%02d.dat", i + 1), nMaxFramesCnt);
			if (detectData.isDetectable()) {
				GlobalValues._matchingData.add(detectData);
			} else {
				break;
			}
		}

		Log.i("BraciPro",
				"Loaded data to match " + GlobalValues._matchingData.size());
		g_strOldMatchingPath = strDirPath;
	}

	public static void initRecordedData(Context pContext, int profileMode) {
		// Recorded sounds data

		SharedPreferencesMgr phoneDb = new SharedPreferencesMgr(pContext);
		GlobalValues.g_nCurDetectingDeafSndType = phoneDb
				.getCurDeafSndType(profileMode);
		if (GlobalValues.g_nCurDetectingDeafSndType >= 0) {
			GlobalValues.g_nCurDetectingDeafSndIndies = phoneDb
					.loadDeafSelectedSndIndices(profileMode);
		}

		GlobalValues.recordedDetectData = new Object[PRJCONST.REC_SOUND_TYPE_CNT];
		for (int type = 0; type < PRJCONST.REC_SOUND_TYPE_CNT; type++) {
			ArrayList<DetectingData> detectDataList = new ArrayList<DetectingData>();
			DetectingData detectData = new DetectingData();
			if (GlobalValues.g_nCurDetectingDeafSndType >= 0) {
				if (GlobalValues.g_nCurDetectingDeafSndIndies[type] >= 0) {
					detectData
							.LoadDetectData(
									PRJFUNC.getDeafSoundDir(
											GlobalValues.g_nCurDetectingDeafSndType,
											type),
									String.format(
											"%02d.dat",
											GlobalValues.g_nCurDetectingDeafSndIndies[type] + 1),
									SoundEngine.MAX_DETECT_EXTRA_SOUNDS_FRAME_CNT[7]);
				} else {
					continue;
				}
			} else {
				detectData.LoadDetectData(
						PRJFUNC.getRecordDir(type, profileMode), "Detect.dat",
						-1);
			}
			if (detectData.isDetectable()) {
				detectDataList.add(detectData);
			} else {
				continue;
			}
			GlobalValues.recordedDetectData[type] = detectDataList;
		}

	}

	public static void initDetectData(Context pContext) {
		// registered sounds data
		String strRootDetectDir = SoundEngine.RECORD_DIR + "/"
				+ PRJFUNC.DETECT_TEMP_PATH;

		// releaseDetectDataFromDb(pContext);

		// Fire data
		if (GlobalValues._fireDetectData == null) {
			GlobalValues._fireDetectData = new ArrayList<DetectingData>();
			for (int i = 0; i < PRJCONST.MAX_FIRE_SOUND_CNT; i++) {
				DetectingData detectData = new DetectingData();
				String strDir = strRootDetectDir + "/"
						+ SIGNAL_NAMES[ConvSignalToInt(Signal.FIRE_ALARM)];
				detectData.LoadDetectData(strDir, "Detect" + i + ".dat",
						SoundEngine.MAX_DETECT_EXTRA_SOUNDS_FRAME_CNT[5]);
				if (detectData.isDetectable()) {
					GlobalValues._fireDetectData.add(detectData);
				} else {
					break;
				}
			}
		}

		// Thief
		if (GlobalValues._thiefDetectData == null) {
			GlobalValues._thiefDetectData = new ArrayList<DetectingData>();
			for (int i = 0; i < PRJCONST.MAX_FIRE_SOUND_CNT; i++) {
				DetectingData detectData = new DetectingData();
				String strDir = strRootDetectDir + "/"
						+ SIGNAL_NAMES[ConvSignalToInt(Signal.THIEF_ALARM)];
				detectData.LoadDetectData(strDir, "Detect" + i + ".dat",
						SoundEngine.MAX_DETECT_EXTRA_SOUNDS_FRAME_CNT[6]);
				if (detectData.isDetectable()) {
					GlobalValues._thiefDetectData.add(detectData);
				} else {
					break;
				}
			}
		}
		// Car Horns
		if (GlobalValues._carHornsDetectData == null) {
			GlobalValues._carHornsDetectData = new ArrayList<DetectingData>();
			for (int i = 0; i < PRJCONST.MAX_FIRE_SOUND_CNT; i++) {
				DetectingData detectData = new DetectingData();
				String strDir = strRootDetectDir + "/"
						+ SIGNAL_NAMES[ConvSignalToInt(Signal.CAR_HORN)];
				detectData.LoadDetectData(strDir, "Detect" + i + ".dat",
						SoundEngine.MAX_DETECT_EXTRA_SOUNDS_FRAME_CNT[0]);
				if (detectData.isDetectable()) {
					GlobalValues._carHornsDetectData.add(detectData);
				} else {
					break;
				}
			}
		}

		// Police
		if (GlobalValues._policeDetectData == null) {
			GlobalValues._policeDetectData = new ArrayList<DetectingData>();
			for (int i = 0; i < PRJCONST.MAX_FIRE_SOUND_CNT; i++) {
				DetectingData detectData = new DetectingData();
				String strDir = strRootDetectDir + "/"
						+ SIGNAL_NAMES[ConvSignalToInt(Signal.POLICE_SIREN)];
				detectData.LoadDetectData(strDir, "Detect" + i + ".dat",
						SoundEngine.MAX_DETECT_EXTRA_SOUNDS_FRAME_CNT[1]);
				if (detectData.isDetectable()) {
					GlobalValues._policeDetectData.add(detectData);
				} else {
					break;
				}
			}
		}

		// Traffic Tone
		if (GlobalValues._trafficToneDetectData == null) {
			GlobalValues._trafficToneDetectData = new ArrayList<DetectingData>();
			for (int i = 0; i < PRJCONST.MAX_FIRE_SOUND_CNT; i++) {
				DetectingData detectData = new DetectingData();
				String strDir = strRootDetectDir + "/"
						+ SIGNAL_NAMES[ConvSignalToInt(Signal.TRAFFIC_TONE)];
				detectData.LoadDetectData(strDir, "Detect" + i + ".dat",
						SoundEngine.MAX_DETECT_EXTRA_SOUNDS_FRAME_CNT[3]);
				if (detectData.isDetectable()) {
					GlobalValues._trafficToneDetectData.add(detectData);
				} else {
					break;
				}
			}
		}

		// Train Horn
		if (GlobalValues._trainHornDetectData == null) {
			GlobalValues._trainHornDetectData = new ArrayList<DetectingData>();
			for (int i = 0; i < PRJCONST.MAX_FIRE_SOUND_CNT; i++) {
				DetectingData detectData = new DetectingData();
				String strDir = strRootDetectDir + "/"
						+ SIGNAL_NAMES[ConvSignalToInt(Signal.TRAIN_WHISTLE)];
				detectData.LoadDetectData(strDir, "Detect" + i + ".dat",
						SoundEngine.MAX_DETECT_EXTRA_SOUNDS_FRAME_CNT[2]);
				if (detectData.isDetectable()) {
					GlobalValues._trainHornDetectData.add(detectData);
				} else {
					break;
				}
			}
		}

		// MicroWave
//		if (GlobalValues._microwaveDetectData == null) {
//			GlobalValues._microwaveDetectData = new ArrayList<DetectingData>();
//			for (int i = 0; i < PRJCONST.MAX_FIRE_SOUND_CNT; i++) {
//				DetectingData detectData = new DetectingData();
//				String strDir = strRootDetectDir + "/"
//						+ SIGNAL_NAMES[ConvSignalToInt(Signal.MICROWAVE)];
//				detectData.LoadDetectData(strDir, "Detect" + i + ".dat",
//						SoundEngine.MAX_DETECT_EXTRA_SOUNDS_FRAME_CNT[4]);
//				if (detectData.isDetectable()) {
//					GlobalValues._microwaveDetectData.add(detectData);
//				} else {
//					break;
//				}
//			}
//		}

		/*
		 * // Alarm Clock if (GlobalValues._alarmClockDetectData == null) {
		 * GlobalValues._alarmClockDetectData = new ArrayList<DetectingData>();
		 * for (int i = 0; i < PRJCONST.MAX_FIRE_SOUND_CNT; i++) { DetectingData
		 * detectData = new DetectingData(); String strDir = strRootDetectDir +
		 * "/" + SIGNAL_NAMES[ConvSignalToInt(Signal.ALARM_CLOCK)];
		 * detectData.LoadDetectData(strDir, "Detect" + i + ".dat"); if
		 * (detectData.isDetectable()) {
		 * GlobalValues._alarmClockDetectData.add(detectData); } } }
		 * 
		 * // Mobile Ring if (GlobalValues._phoneRingDetectData == null) {
		 * GlobalValues._phoneRingDetectData = new ArrayList<DetectingData>();
		 * for (int i = 0; i < PRJCONST.MAX_FIRE_SOUND_CNT; i++) { DetectingData
		 * detectData = new DetectingData(); String strDir = strRootDetectDir +
		 * "/" + SIGNAL_NAMES[ConvSignalToInt(Signal.MOBILE_RINGING)];
		 * detectData.LoadDetectData(strDir, "Detect" + i + ".dat"); if
		 * (detectData.isDetectable()) {
		 * GlobalValues._phoneRingDetectData.add(detectData); } } }
		 * 
		 * // Doorbell data if (GlobalValues._extraDoorbellDetectData == null) {
		 * GlobalValues._extraDoorbellDetectData = new
		 * ArrayList<DetectingData>(); for (int i = 0; i <
		 * PRJCONST.MAX_FIRE_SOUND_CNT; i++) { DetectingData detectData = new
		 * DetectingData(); String strDir = strRootDetectDir + "/" +
		 * SIGNAL_NAMES[ConvSignalToInt(Signal.DOOR_BELL)];
		 * detectData.LoadDetectData(strDir, "Detect" + i + ".dat"); if
		 * (detectData.isDetectable()) {
		 * GlobalValues._extraDoorbellDetectData.add(detectData); } } }
		 * 
		 * File dir = new File(strRootDetectDir); if (dir.isDirectory()) {
		 * String[] children = dir.list(); for (int i = 0; i < children.length;
		 * i++) { File dir1 = new File(strRootDetectDir + "/" + children[i]);
		 * String[] children1 = dir1.list(); for (int j = 0; j <
		 * children1.length; j++) { new File(dir1, children1[j]).delete(); } new
		 * File(dir, children[i]).delete(); } } dir.delete();
		 */
	}

	public static String getDeafRootDir() {
		return SoundEngine.RECORD_DIR + "/DeafProducts";
	}

	public static String getDeafSoundDir(int p_nDeafType, int p_nSndType) {
		return getDeafRootDir() + "/" + STR_DEAF_PATHS[p_nDeafType] + "/"
				+ STR_DEAF_SOUND_TYPES[p_nSndType];
	}

	public static void extractDeafData(Context p_context) {
		String strAssetRootDir = "DeafProducts";
		for (int idx = 0; idx < PRJCONST.DEAF_TYPE_CNT; idx++) {
			for (int type = 0; type < PRJCONST.REC_SOUND_TYPE_CNT; type++) {
				String strIntermediatePath = "/" + STR_DEAF_PATHS[idx] + "/"
						+ STR_DEAF_SOUND_TYPES[type];
				String strDeafSoundDir = getDeafSoundDir(idx, type);
				int i = 1;
				while (true) {
					String strFileName = String.format("%02d.dat", i);
					if (!copyFileFromAsset(strAssetRootDir
							+ strIntermediatePath + "/" + strFileName,
							strDeafSoundDir, strFileName, p_context)) {
						break;
					}
					i++;
				}
			}
		}
	}

	private static boolean copyFileFromAsset(String p_strAssetPath,
			String p_strDstDir, String p_strFileName, Context p_context) {
		try {

			InputStream is = p_context.getAssets().open(p_strAssetPath);
			int size = is.available();
			byte[] buffer = new byte[size];
			is.read(buffer);
			is.close();

			File dir = new File(p_strDstDir);
			if (!dir.exists())
				dir.mkdirs();

			File f = new File(p_strDstDir + "/" + p_strFileName);
			FileOutputStream fos = new FileOutputStream(f);
			fos.write(buffer);
			fos.close();
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public static void showMessagePopup(Activity prev, int dlgId, int iconres,
			String title, String message, String btnText) {
		Intent intent = new Intent(prev, DlgYesNo.class);
		intent.putExtra("ICON_RES", iconres);
		intent.putExtra("TITLE", title);
		intent.putExtra("MESSAGE", message);
		intent.putExtra("OK", btnText);
		intent.putExtra("CANCEL", "Cancel");
		prev.startActivityForResult(intent, dlgId);
	}

	public static String convertEmailToChannelStr(String strEmail) {
		if (strEmail == null || strEmail.isEmpty())
			return strEmail;
		String str = strEmail.replace('.', '-');
		str = str.replace('@', '-');
		return str;
	}

	public static String getRecordRootDir(int p_nProfileMode) {
		String strRecDir = "";
		if (p_nProfileMode == PRJCONST.PROFILE_MODE_HOME)
			strRecDir = SoundEngine.RECORD_DIR + "/home";
		else
			strRecDir = SoundEngine.RECORD_DIR + "/office";

		return strRecDir;
	}

	public static String getRecordDir(int p_nRecSoundType, int p_nProfileMode) {
		String strRecDir = "";
		strRecDir = getRecordRootDir(p_nProfileMode) + "/" + p_nRecSoundType;

		return strRecDir;
	}

	public static ArrayList<DetectingData> getRecordingDataList(
			int p_nRecSoundType) {
		if (GlobalValues.recordedDetectData == null)
			return null;

		return (ArrayList<DetectingData>) GlobalValues.recordedDetectData[p_nRecSoundType];
	}

	public static String getRecordingTitle(int p_nRecSndType, Context context) {
		String strTitle = "";
		switch (p_nRecSndType) {
		case PRJCONST.REC_SOUND_TYPE_DOORBELL:
			strTitle = context.getString(R.string.record_prev_title_doorbell);
			break;
		case PRJCONST.REC_SOUND_TYPE_INTERCOM:
			strTitle = context.getString(R.string.record_prev_title_intercom);
			break;
		case PRJCONST.REC_SOUND_TYPE_LINEPHONE:
			strTitle = context.getString(R.string.record_prev_title_linephone);
			break;
		case PRJCONST.REC_SOUND_TYPE_SMOKEALARM:
			strTitle = context.getString(R.string.record_prev_title_smoke);
			break;
		default:
			break;
		}
		return strTitle;
	}

	public static void removeFragment(FragmentActivity activity, Fragment fg) {
		if (fg == null)
			return;

		FragmentManager fm = activity.getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		ft.remove(fg);
		ft.commit();
	}

	public static void addFragment(FragmentActivity activity, Fragment fg) {
		if (fg == null)
			return;

		FragmentManager fm = activity.getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		ft.setCustomAnimations(R.anim.fade, R.anim.hold);
		ft.add(R.id.frm_contents, fg);
		ft.commit();
	}

	public static void showFragment(FragmentActivity activity, Fragment fg) {
		if (fg == null)
			return;

		FragmentManager fm = activity.getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		ft.setCustomAnimations(R.anim.fade, R.anim.hold);
		ft.show(fg);
		ft.commit();
	}

	public static void hideFragment(FragmentActivity activity, Fragment fg) {
		if (fg == null)
			return;

		FragmentManager fm = activity.getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		// ft.setCustomAnimations(R.anim.fade, R.anim.hold);
		ft.hide(fg);
		ft.commit();
	}

	private static Dialog m_dlgProgress;
	private static Context m_dlgProgressContext;

	public static void showProgress(Context context, String strMsg) {
		if (m_dlgProgress != null) {
			return;
		}
		if (strMsg == null) {
			strMsg = "";
		}
		closeProgress(null);
		try {
			m_dlgProgressContext = context;
			m_dlgProgress = ProgressDialog.show(context, null, strMsg, true,
					false);

			// m_dlgProgress = ProgressDialog.show(this, null, strMsg, true,
			// true,
			// new DialogInterface.OnCancelListener() {
			// public void onCancel(DialogInterface arg0) {
			// ;
			// }
			// });
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void closeProgress() {
		closeProgress(null);
	}

	public static void closeProgress(Context p_context) {
		if (m_dlgProgress == null) {
			return;
		}

		if (p_context != null && m_dlgProgressContext != p_context)
			return;

		m_dlgProgressContext = null;
		m_dlgProgress.dismiss();
		m_dlgProgress = null;

	}

	public static boolean setBabyCryingThreshold(Context p_context) {
		// SharedPreferencesMgr phoneDb = new SharedPreferencesMgr(p_context);
		// float fDevSensitivity = phoneDb.loadDevSensitivity();
		// if (fDevSensitivity <= 0)
		// return false;

		// for testing
		float fDevSensitivity = 60.0f;

		// if (fDevSensitivity > 400) {
		// SoundEngine.BABYCRYING_THRESHOLD = 30.0f;
		// } else if (fDevSensitivity > 200) {
		// SoundEngine.BABYCRYING_THRESHOLD = 20.0f;
		// } else if (fDevSensitivity > 130) {
		// SoundEngine.BABYCRYING_THRESHOLD = 10.0f;
		// } else if (fDevSensitivity > 50) {
		// SoundEngine.BABYCRYING_THRESHOLD = 5.0f;
		// } else if (fDevSensitivity > 10) {
		// SoundEngine.BABYCRYING_THRESHOLD = 2.0f;
		// } else {
		// SoundEngine.BABYCRYING_THRESHOLD = 1.0f;
		// }

		return true;
	}

	public static void updateProfileMode(Context pContext) {
		if (PRJFUNC.gpsTracker == null) {
			PRJFUNC.gpsTracker = new GPSTracker(pContext);
		}

		boolean bInOffice = false;
		boolean bInHome = false;
		double distance = 0;

		Location locationCur = PRJFUNC.gpsTracker.getLocation();

		if (GlobalValues.location_home_lng != PRJCONST.INVALID_LOCATION_VALUE) {
			Location locationHome = new Location("home");
			locationHome.setLatitude(GlobalValues.location_home_lat);
			locationHome.setLongitude(GlobalValues.location_home_lng);

			distance = locationCur.distanceTo(locationHome);
			if (distance < 50) {
				bInHome = true;
			}
		}

		if (GlobalValues.location_office_lng != PRJCONST.INVALID_LOCATION_VALUE) {
			Location locationHome = new Location("office");
			locationHome.setLatitude(GlobalValues.location_office_lat);
			locationHome.setLongitude(GlobalValues.location_office_lng);

			distance = locationCur.distanceTo(locationHome);
			if (distance < 50) {
				bInOffice = true;
			}
		}

		boolean bOldProfileIndoor = GlobalValues.m_bProfileIndoor;
		int nOldProfileIndoorMode = GlobalValues.m_nProfileIndoorMode;

		int nMode = -1;
		if (!bInOffice && !bInHome) {
			nMode = GlobalValues.m_nProfileOutdoorMode;
		} else {
			GlobalValues.m_bProfileIndoor = true;
			if (bInOffice != bInHome) {
				if (bInOffice) {
					nMode = PRJCONST.PROFILE_MODE_OFFICE;
				} else if (bInHome) {
					nMode = PRJCONST.PROFILE_MODE_HOME;
				}
			}
		}

		if (nMode >= 0) {
			setProfileMode(pContext, nMode);
		}
	}

	public static boolean setProfileMode(Context p_Context, int p_nMode) {
		if (getCurProfileMode() == p_nMode)
			return false;

		SharedPreferencesMgr phoneDb = new SharedPreferencesMgr(p_Context);

		boolean bIndoor = isIndoor(p_nMode);
		if (bIndoor) {
			if (p_nMode != GlobalValues.m_nProfileIndoorMode) {
				PRJFUNC.initRecordedData(p_Context, p_nMode);
				SoundEngine.THIEF_ALARM_IDX = phoneDb
						.loadTheifAlarmIndex(p_nMode);
			}
			GlobalValues.m_nProfileIndoorMode = p_nMode;
		} else {
			GlobalValues.m_nProfileOutdoorMode = p_nMode;
		}
		GlobalValues.m_bProfileIndoor = bIndoor;

		SoundEngine.DELTA_MATCH_RATES = phoneDb.loadMatchingRatesDelta(p_nMode);

		if (g_homeActivity != null) {
			g_homeActivity.m_handlerProfile.sendEmptyMessage(0);
		}

		phoneDb.saveProfileMode();
		if (bIndoor) {
			phoneDb.saveProfileIndoorMode();
		} else {
			phoneDb.saveProfileOutdoorMode();
		}

		return true;
	}

	public static boolean haveMultiData(int p_nDeafSndType, int p_nSndType) {
		String strDirPath = PRJFUNC.getDeafSoundDir(p_nDeafSndType, p_nSndType);
		File secondFile = new File(strDirPath, "02.dat");
		if (secondFile.exists())
			return true;

		return false;
	}

	public static int getCurProfileMode() {
		if (GlobalValues.m_bProfileIndoor)
			return GlobalValues.m_nProfileIndoorMode;
		else
			return GlobalValues.m_nProfileOutdoorMode;
	}

	public static boolean isIndoor(int p_nMode) {
		if (p_nMode == PRJCONST.PROFILE_MODE_HOME
				|| p_nMode == PRJCONST.PROFILE_MODE_OFFICE)
			return true;
		return false;
	}

}
