package com.uc.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SQLDBHelper extends SQLiteOpenHelper {

	private final String TAG = "SQLDBHelper";

	public SQLDBHelper(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		// arg0.execSQL("CREATE TABLE " + MsgDbMgrMy.MessageData.TABLE_NAME +
		// " ("
		// + " _id INTEGER PRIMARY KEY AUTOINCREMENT, "
		// + MsgDbMgrMy.MessageData.C_THREAD_ID + " INTEGER, "
		// + MsgDbMgrMy.MessageData.C_ADDRESS + " TEXT, "
		// + MsgDbMgrMy.MessageData.C_DATE + " INTEGER, "
		// + MsgDbMgrMy.MessageData.C_READ + " INTEGER, "
		// + MsgDbMgrMy.MessageData.C_MSG_BOX + " INTEGER, "
		// + MsgDbMgrMy.MessageData.C_TITLE + " TEXT, "
		// + MsgDbMgrMy.MessageData.C_BODY + " TEXT, "
		// + MsgDbMgrMy.MessageData.C_SERVICE_CENTER + " TEXT, "
		// + MsgDbMgrMy.MessageData.C_MID + " INTEGER, "
		// + MsgDbMgrMy.MessageData.C_STATUS + " INTEGER, "
		// + MsgDbMgrMy.MessageData.C_DATA1 + " TEXT, "
		// + MsgDbMgrMy.MessageData.C_DATA2 + " TEXT, "
		// + MsgDbMgrMy.MessageData.C_DATA3 + " TEXT, "
		// + MsgDbMgrMy.MessageData.C_DATA4 + " TEXT, "
		// + MsgDbMgrMy.MessageData.C_DATA5 + " TEXT);");
	}

	// 존재하는 DB 버전과 다른 경우
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		Log.d(TAG, "Upgrading database from version " + oldVersion + " to "
				+ newVersion + ", which will destory all old data");
		// 이전 버전 DB 삭제하고 다시 생성
		// db.execSQL("DROP TABLE " + DBTableContacts.TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS data");
		onCreate(db);
	}
}
