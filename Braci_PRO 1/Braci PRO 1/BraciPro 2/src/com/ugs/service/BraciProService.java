package com.ugs.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

import com.uc.prjcmn.PRJFUNC;
import com.uc.prjcmn.SharedPreferencesMgr;
import com.ugs.braci.DoorbellDetectedActivity;
import com.ugs.braci.GlobalValues;
import com.ugs.braci.HomeActivity;
import com.ugs.braci.MainApplication;
import com.ugs.braci._SplashActivity;
import com.ugs.braci.R;
import com.ugs.braci.engine.SoundEngine;
import com.ugs.lib.GPSTracker;

public class BraciProService extends Service {

	NotificationManager notiManager;
	public static final int MyNoti = 30056;

	private boolean m_bNotiBackground = false;
	int times = 0;
	public static int times_for_location = 0;

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub

		return null;
	}

	@Override
	public void onCreate() {

		super.onCreate();

		GlobalValues._myService = BraciProService.this;

		// SoundEngine soundEngine = new SoundEngine();
		// if (soundEngine.isRunning())
		// GlobalValues._soundEngine = soundEngine;
		// else
		// GlobalValues._soundEngine = null;
		//

		notiManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				running();
			}
		}, 10000);
	}

	private void registerNotificationIcon() {
		if (!m_bNotiBackground) {
			Notification notification = new Notification(
					R.drawable.ic_launcher, "Detecting Sound",
					System.currentTimeMillis());

			PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
					new Intent(this, HomeActivity.class),
					PendingIntent.FLAG_UPDATE_CURRENT);

			notification.setLatestEventInfo(this, "Braci",
					"Braci is detecting sounds.", contentIntent);

			startForeground(33333, notification);
			m_bNotiBackground = true;
		}
	}

	@Override
	public void onStart(Intent intent, int startId) {
		if (notiManager != null) {
			notiManager.cancelAll();
		}

		if (GlobalValues._soundEngine != null && GlobalValues.m_bDetect) {
			registerNotificationIcon();
		}

		super.onStart(intent, startId);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if (GlobalValues._soundEngine != null && GlobalValues.m_bDetect) {
			registerNotificationIcon();
		}

		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public void onDestroy() {
		if (m_bNotiBackground) {
			stopForeground(true);
		}

		if (GlobalValues._soundEngine != null) {
			GlobalValues._soundEngine.Terminate();
			GlobalValues._soundEngine = null;
		}

		GlobalValues._myService = null;

		super.onDestroy();
	}

	public Handler m_handler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case GlobalValues.COMMAND_OPEN_ACITIVITY:
				if (!GlobalValues._bEnabledActivity)
					createActivity((PRJFUNC.Signal) msg.obj);
				if (GlobalValues.m_bNotifyAndroidWear) {
					notifyAlarm((PRJFUNC.Signal) msg.obj);
				}
				break;
			case GlobalValues.COMMAND_PUSH_NOTIFICATION:
				PRJFUNC.Signal signal = PRJFUNC.ConvIntToSignal((Integer)msg.obj);
				if (GlobalValues.m_bNotifyAndroidWear) {
					notifyAlarm(signal);
				}
				PRJFUNC.sendPushMessage(BraciProService.this, signal);
				createActivity(signal);
				break;
			case GlobalValues.COMMAND_SEND_PEBBLE:
				PRJFUNC.sendSignalToPebble(BraciProService.this,
						(PRJFUNC.Signal) msg.obj);
				break;
			case GlobalValues.COMMAND_ENABLE_DETECT:
				if (GlobalValues._soundEngine != null && GlobalValues.m_bDetect) {
					registerNotificationIcon();
				}
				break;
			case GlobalValues.COMMAND_DISABLE_DETECT:
				if (m_bNotiBackground) {
					stopForeground(true);
					m_bNotiBackground = false;
				}
				break;
			case GlobalValues.COMMAND_REMOVE_NOTIFY:
				notiManager.cancel(MyNoti);
				break;
				
			case GlobalValues.COMMAND_SYSTEM_EXIT:
				if (m_bNotiBackground) {
					stopForeground(true);
					m_bNotiBackground = false;
				}
				System.exit(0);
				break;
			default:
				break;
			}
		}
	};

	private void createActivity(PRJFUNC.Signal p_signal) {
		// PushWakeLock.acquireCpuWakeLock(DoorbellService.this);

		Intent intent = null;

		intent = new Intent(BraciProService.this, DoorbellDetectedActivity.class);
		intent.putExtra(DoorbellDetectedActivity.EXTRA_PARAM,
				PRJFUNC.ConvSignalToInt(p_signal));

		if (intent != null) {
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			BraciProService.this.startActivity(intent);
		}

		// if (GlobalValues._myService.m_handler != null) {
		// Message msg = new Message();
		// msg.what = GlobalValues.COMMAND_SEND_PEBBLE;
		// msg.obj = p_signal;
		// GlobalValues._myService.m_handler
		// .sendMessage(msg);
		// }
		PRJFUNC.sendSignalToPebble(BraciProService.this, p_signal);
	}

	private void notifyAlarm(PRJFUNC.Signal p_signal) {
		// notification 객체 생성(상단바에 보여질 아이콘, 메세지, 도착시간 정의)
		Notification noti = new Notification(R.drawable.ic_launcher// 알림창에 띄울
																	// 아이콘
				, "Sound Detected!", // 간단 메세지
				System.currentTimeMillis()); // 도착 시간
		// 기본으로 지정된 소리를 내기 위해
		noti.defaults = Notification.DEFAULT_SOUND;
		// 알림 소리를 한번만 내도록
		noti.flags = Notification.FLAG_ONLY_ALERT_ONCE;
		// 확인하면 자동으로 알림이 제거 되도록
		noti.flags = Notification.FLAG_AUTO_CANCEL;
		// 사용자가 알람을 확인하고 클릭했을때 새로운 액티비티를 시작할 인텐트 객체
		Intent intent = null;
		String strCommand = getCommentWithSignal(p_signal);
		intent = new Intent(BraciProService.this, _SplashActivity.class);

		if (intent == null)
			return;

		// 새로운 태스크(Task) 상에서 실행되도록(보통은 태스크1에 쌓이지만 태스크2를 만들어서 전혀 다른 실행으로 관리한다)
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		// 인텐트 객체를 포장해서 전달할 인텐트 전달자 객체
		PendingIntent pendingI = PendingIntent.getActivity(BraciProService.this,
				0, intent, 0);
		// 받아온 메시지 담기
		String arrivedMsg = strCommand;
		// 상단바를 드래그 했을때 보여질 내용 정의하기
		noti.setLatestEventInfo(BraciProService.this, "[Braci Pro]", arrivedMsg,
				pendingI);
		// 알림창 띄우기(알림이 여러개일수도 있으니 알림을 구별할 상수값, 여러개라면 상수값을 달리 줘야 한다.)
		notiManager.notify(MyNoti, noti);

		//
		// PRJFUNC.showPushPopup(BtService.this, "Command " + strCommand);
	}

	private String getCommentWithSignal(PRJFUNC.Signal p_signal) {
		String strContent = "Special sound is heard.";
		
		int[] nStrIds = {R.string.sound_detect_fire, R.string.sound_detect_doorbell, R.string.babycrying_detected_title,
				R.string.sound_detect_phonering, R.string.sound_detect_carhorn, R.string.sound_detect_thief,
				R.string.sound_detect_alarmclock, R.string.paging_wakeup_title, R.string.paging_bed_title,
				R.string.paging_sos_title, R.string.paging_meal_title, R.string.paging_no_title,
				R.string.paging_yes_title, R.string.paging_call_title, R.string.traffictone_detected_title,
				R.string.train_detected_title, R.string.microwave_detected_title, R.string.police_detected_title,
				R.string.intercom_detected_title, R.string.dogbark_detected_title, R.string.snore_detected_title,
				R.string.smoke_detected_title, R.string.fire_detected_title, R.string.backdoorbell_detected_title,
		};
		
		int nSignal = PRJFUNC.ConvSignalToInt(p_signal);
		if (nSignal >= 0) {
			strContent = getString(nStrIds[nSignal]);
		}

		return strContent;
	}

	void running() {
		times++;
		if (times >= 60)
			times = 0;
		if (times % 6 == 0) {
			SharedPreferencesMgr pPhoneDb = ((MainApplication) getApplication())
					.getSharedPreferencesMgrPoint();

			boolean bDetecting = pPhoneDb.IsDetectionMode();

			if (bDetecting) {
				if (GlobalValues._soundEngine == null)
					GlobalValues._soundEngine = new SoundEngine();

				if (!GlobalValues.m_bDetect)
					GlobalValues.m_bDetect = true;
			}
			times = 0;
		}

		if (GlobalValues._soundEngine != null && GlobalValues.m_bDetect) {
			registerNotificationIcon();
		} else {
			if (m_bNotiBackground) {
				stopForeground(true);
				m_bNotiBackground = false;
			}
		}

		if (GlobalValues.m_bProfileAutoSel) {
			if (times_for_location == 0) {
				PRJFUNC.updateProfileMode(BraciProService.this);
			} 
			
			times_for_location += 10;
			if (times_for_location >= SoundEngine.UPDATE_LOCATION_TIME)
				times_for_location = 0;
		}

		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				running();
			}
		}, 10000);
	}

//	void checkDetectionMode() {
//		ConnectivityManager conn_manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
//
//		GlobalValues.m_bProfileIndoor = false;
//		try {
//			NetworkInfo network_info = conn_manager.getActiveNetworkInfo();
//			if (network_info != null && network_info.isConnected()) {
//
//				if (network_info.getType() == ConnectivityManager.TYPE_WIFI) {
//					GlobalValues.m_bProfileIndoor = true;
//				}
//			}
//
//		} catch (Exception e) {
//			Log.d("HttpRequester", e.getMessage());
//		}
//	}
}
