package com.ugs.braci;

import java.io.File;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings.Global;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseAnalytics;
import com.parse.ParsePush;
import com.parse.ParseUser;
import com.uc.popup.DlgExit;
import com.uc.prjcmn.ActivityTask;
import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.uc.prjcmn.RS;
import com.uc.prjcmn.SharedPreferencesMgr;
import com.ugs.braci.engine.DetectingData;
import com.ugs.braci.engine.SoundEngine;
import com.ugs.braci.help.HelpAboutUsActivity;
import com.ugs.braci.help.HelpFeedbackActivity;
import com.ugs.braci.pagingsystem.PagingSystemActivity;
import com.ugs.braci.pebble.PebbleActivity;
import com.ugs.braci.profile.ProfileModeActivity;
import com.ugs.braci.settings.AdvancedSettingsActivity;
import com.ugs.braci.setup.SetupActivity;
import com.ugs.braci.R;
import com.ugs.equalizer.EqualizerView;
import com.ugs.lib.GPSTracker;
import com.ugs.lib.OnSwipeTouchListener;
import com.ugs.scrollmenu.ClickListenerForScrolling;
import com.ugs.scrollmenu.MyHorizontalScrollView;
import com.ugs.scrollmenu.SizeCallbackForMain;

public class HomeActivity extends Activity implements OnClickListener {

	private final String TAG = "_HomeActivity";

	private final int DLG_EXIT = 1;

	private Context mContext;

	private MyHorizontalScrollView _mScrollView;
	private View _mApp, _mLeftMenu, _mRightMenu;
	private ClickListenerForScrolling _mLeftListener;
	private ClickListenerForScrolling _mRightListener;

	private ImageView m_ivLeftMenu;
	private ImageView m_ivRightMenu;

	private ImageView m_ivBlankBuf;

	private ImageView m_ivDetectMode;
	private TextView m_tvDetectMode;

	// For profile mode
	private ImageView m_ivProfileHome;
	private ImageView m_ivProfileHomeSel;
	private ImageView m_ivProfileOffice;
	private ImageView m_ivProfileOfficeSel;
	private ImageView m_ivProfileDrive;
	private ImageView m_ivProfileDriveSel;
	private ImageView m_ivProfileWalk;
	private ImageView m_ivProfileWalkSel;
	private TextView m_tvProfileMode;

	private EqualizerView m_equalizerView;

	private ImageView m_ivTutorial;

	OnSwipeTouchListener swipeTouchListener = null;

	// For pebble connection
	private ImageView m_ivPebbleConState;
	private ImageView m_ivPebbleWatch;

	// 터치 불가능한 령역 설정.
	enum BlankSize {
		None, Whole,
	}

	// ////////////////////////////////////////////////////////////
	// //////////////////////////////////////////////////////////////
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		LayoutInflater inflater = LayoutInflater.from(this);
		_mScrollView = (MyHorizontalScrollView) inflater.inflate(
				R.layout.horz_scroll_with_list_menu, null);

		_mLeftMenu = inflater.inflate(R.layout.menu_main_left, null);
		_mApp = inflater.inflate(R.layout.activity_home, null);
		_mRightMenu = inflater.inflate(R.layout.menu_main_right, null);

		_mLeftListener = new ClickListenerForScrolling(_mScrollView,
				_mLeftMenu, 0);
		_mRightListener = new ClickListenerForScrolling(_mScrollView,
				_mRightMenu, 2);

		m_ivLeftMenu = (ImageView) _mApp.findViewById(R.id.iv_main_menu);
		_mLeftListener.setCbOnMenuOpened(m_leftMenuCb);
		m_ivLeftMenu.setOnClickListener(_mLeftListener);

		m_ivRightMenu = (ImageView) _mApp.findViewById(R.id.iv_help_menu);
		_mRightListener.setCbOnMenuOpened(m_rightMenuCb);
		m_ivRightMenu.setOnClickListener(_mRightListener);

		final View[] children = new View[] { _mLeftMenu, _mApp, _mRightMenu };
		int scrollToViewIdx = 1;
		int left_width = (int) (RS.X_Z * 480);
		int right_width = (int) (RS.X_Z * 430);
		int[] widths = new int[] { left_width, PRJFUNC.W_LCD, right_width };

		_mScrollView.initViews(children, scrollToViewIdx,
				new SizeCallbackForMain(widths));

		setContentView(_mScrollView);

		ActivityTask.INSTANCE.add(this);

		mContext = (Context) this;

		PRJFUNC.testPebbleConnected(mContext);

		updateLCD();

		// - update position
		if (!PRJFUNC.DEFAULT_SCREEN) {
			scaleView();
		}

		SharedPreferencesMgr phoneDb = new SharedPreferencesMgr(mContext);
		if (phoneDb.isFirstHomeScreen()) {

			m_ivTutorial.setVisibility(View.VISIBLE);

			GlobalValues._firstRun = false;

			deleteAllAlreadyRecords();

			Intent intent = new Intent(HomeActivity.this, SetupActivity.class);
			intent.putExtra(SetupActivity.AUTO_OPENED, true);
			startActivity(intent);
			overridePendingTransition(R.anim.right_in, R.anim.hold);
		}

		GlobalValues._bExistHomeActivity = true;

		if (GlobalValues.m_stUserName == null
				|| GlobalValues.m_stUserName.isEmpty()) {
			SharedPreferencesMgr pPhoneDb = new SharedPreferencesMgr(mContext);
			pPhoneDb.loadUserInfo();
		}

		try {
			if (PRJCONST.IsInternetVersion) {
				ParsePush.subscribeInBackground(PRJFUNC
						.convertEmailToChannelStr(GlobalValues.m_stUserName));
				ParseAnalytics.trackAppOpened(getIntent());
			}

			if (GlobalValues.m_bProfileAutoSel) {
				PRJFUNC.gpsTracker = new GPSTracker(mContext);
				if (!PRJFUNC.gpsTracker.canGetLocation()) {
					PRJFUNC.gpsTracker.showSettingsAlert();
				}
			}
		} catch (Exception e) {
			Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_SHORT).show();
		}
	}

	private void deleteAllAlreadyRecords() {
		File dir = new File(
				PRJFUNC.getRecordRootDir(PRJCONST.PROFILE_MODE_HOME));
		PRJFUNC.deleteFile(dir, false);

		dir = new File(PRJFUNC.getRecordRootDir(PRJCONST.PROFILE_MODE_OFFICE));
		PRJFUNC.deleteFile(dir, false);
	}

	ClickListenerForScrolling.CallbackEvent m_rightMenuCb = new ClickListenerForScrolling.CallbackEvent() {

		@Override
		public void callbackMethod(boolean p_bOpened) {
			if (p_bOpened) {
				setBlankBufSize(BlankSize.Whole);
			} else {
				setBlankBufSize(BlankSize.None);
			}

		}
	};

	ClickListenerForScrolling.CallbackEvent m_leftMenuCb = new ClickListenerForScrolling.CallbackEvent() {

		@Override
		public void callbackMethod(boolean p_bOpened) {
			if (p_bOpened) {
				setBlankBufSize(BlankSize.Whole);
			} else {
				setBlankBufSize(BlankSize.None);
			}
		}
	};

	@Override
	protected void onResume() {
		super.onResume();

		udpateUIForDetectState();
		updateUIForPebble();

		updateProfileModeUI();

		PRJFUNC.g_homeActivity = this;

		GlobalValues._bEnabledActivity = false;

		GlobalValues._equalizerView = m_equalizerView;
		GlobalValues._equalizerView.setDrawable(true);

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == DLG_EXIT) {
			if (resultCode == RESULT_OK) {
				int optionCode = data.getIntExtra(DlgExit.RESULT_STR,
						DlgExit.SELECT_OPT_LOGOUT);
				if (optionCode == DlgExit.SELECT_OPT_LOGOUT) {
					onLogout();
				} else if (optionCode == DlgExit.SELECT_OPT_SWITCHOFF) {
					onSwitchOff();
				} else if (optionCode == DlgExit.SELECT_OPT_RUNINBACK) {
					onRuninbackground();
				}
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	private void udpateUIForDetectState() {
		m_ivDetectMode.setSelected(GlobalValues.m_bDetect);
		if (GlobalValues.m_bDetect) {
			m_tvDetectMode.setText(getString(R.string.home_detect_state_on));
		} else {
			m_tvDetectMode.setText(getString(R.string.home_detect_state_off));
		}
	}

	private void updateUIForPebble() {
		m_ivPebbleConState.setSelected(GlobalValues.m_bPebbleWatch);
		m_ivPebbleWatch.setSelected(GlobalValues.m_bPebbleWatch);
	}

	@Override
	protected void onPause() {

		// _mLeftListener.close();
		// _mRightListener.close();

		PRJFUNC.g_homeActivity = null;

		GlobalValues._equalizerView.setDrawable(false);
		GlobalValues._equalizerView = null;

		super.onPause();
	}

	@Override
	protected void onDestroy() {
		releaseValues();

		ActivityTask.INSTANCE.remove(this);

		super.onDestroy();
		GlobalValues._bExistHomeActivity = false;
	}

	private void releaseValues() {

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			onBack();
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}

	// //////////////////////////////////////////////////
	private void updateLCD() {

		if (PRJFUNC.mGrp == null) {
			PRJFUNC.resetGraphValue(mContext);
		}

		updateLCDForLeft();
		updateLCDForRight();

		swipeTouchListener = new OnSwipeTouchListener(this) {
			@Override
			public void onSwipeRight() {
				int viewIdx = _mScrollView.getViewIdx();
				if (viewIdx == 1) {
					_mLeftListener.open();
				} else if (viewIdx == 2) {
					// right menu is opened.
					_mRightListener.close();
				}
			}

			@Override
			public void onSwipeLeft() {
				int viewIdx = _mScrollView.getViewIdx();
				if (viewIdx == 0) {
					_mLeftListener.close();
				} else if (viewIdx == 1) {
					// right menu is opened.
					_mRightListener.open();
				}
			}

			@Override
			public void onJustTouched() {
				int viewIdx = _mScrollView.getViewIdx();
				if (viewIdx == 0) {
					// left menu is opened.
					_mLeftListener.close();
				} else if (viewIdx == 2) {
					// right menu is opened.
					_mRightListener.close();
				} else {
				}
			}
		};

		m_ivBlankBuf = (ImageView) findViewById(R.id.iv_blank_buffer);
		// m_ivBlankBuf.setOnClickListener(this);
		m_ivBlankBuf.setOnTouchListener(swipeTouchListener);

		// For selecting profile mode
		m_ivProfileHome = (ImageView) findViewById(R.id.iv_profile_home);
		m_ivProfileHomeSel = (ImageView) findViewById(R.id.iv_profile_home_select);
		m_ivProfileOffice = (ImageView) findViewById(R.id.iv_profile_office);
		m_ivProfileOfficeSel = (ImageView) findViewById(R.id.iv_profile_office_select);
		m_ivProfileDrive = (ImageView) findViewById(R.id.iv_profile_drive);
		m_ivProfileDriveSel = (ImageView) findViewById(R.id.iv_profile_drive_select);
		m_ivProfileWalk = (ImageView) findViewById(R.id.iv_profile_walk);
		m_ivProfileWalkSel = (ImageView) findViewById(R.id.iv_profile_walk_select);

		m_tvProfileMode = (TextView) findViewById(R.id.tv_profile);

		// For Detecting
		m_ivDetectMode = (ImageView) findViewById(R.id.iv_detect_mode);
		m_tvDetectMode = (TextView) findViewById(R.id.tv_detect_state);
		PRJFUNC.setTextViewFont(mContext, m_tvDetectMode,
				PRJCONST.FONT_OpenSansBold);

		View _v = null;

		_v = findViewById(R.id.frm_profile_home);
		_v.setOnClickListener(this);
		_v = findViewById(R.id.frm_profile_office);
		_v.setOnClickListener(this);
		_v = findViewById(R.id.frm_profile_drive);
		_v.setOnClickListener(this);
		_v = findViewById(R.id.frm_profile_walk);
		_v.setOnClickListener(this);

		_v = findViewById(R.id.frm_detection_mode);
		_v.setOnClickListener(this);

		m_equalizerView = (EqualizerView) findViewById(R.id.view_equalizer);
		m_equalizerView.onCreate();

		TextView _tv = (TextView) findViewById(R.id.tv_pebble_title);
		PRJFUNC.setTextViewFont(mContext, _tv, PRJCONST.FONT_OpenSansBold);

		m_ivPebbleConState = (ImageView) findViewById(R.id.iv_pebble_connection);
		m_ivPebbleWatch = (ImageView) findViewById(R.id.iv_pebble_watch);

		m_ivTutorial = (ImageView) findViewById(R.id.iv_tutorial);
		m_ivTutorial.setOnClickListener(this);

		View _frmWhole = findViewById(R.id.frm_whole);
		_frmWhole.setOnTouchListener(swipeTouchListener);
		
		View _frmPebble = findViewById(R.id.frm_pebble_watch);
		_frmPebble.setOnClickListener(this);
	}

	private void updateLCDForLeft() {
		TextView _tv = (TextView) _mLeftMenu.findViewById(R.id.tv_user_email);
		if (GlobalValues.m_stUserName == null
				|| GlobalValues.m_stUserName.isEmpty()) {
			_tv.setText("Login");
		} else {
			_tv.setText(GlobalValues.m_stUserName);
		}

		View _v = _mLeftMenu.findViewById(R.id.frm_setup_home);
		_v.setOnClickListener(this);

		_v = _mLeftMenu.findViewById(R.id.frm_profile_mode);
		_v.setOnClickListener(this);

		_v = _mLeftMenu.findViewById(R.id.frm_paging_system);
		_v.setOnClickListener(this);

		_v = _mLeftMenu.findViewById(R.id.frm_pebble);
		_v.setOnClickListener(this);

		_v = _mLeftMenu.findViewById(R.id.frm_advanced_settings);
		_v.setOnClickListener(this);

		_v = _mLeftMenu.findViewById(R.id.frm_exit);
		_v.setOnClickListener(this);
	}

	private void updateLCDForRight() {
		ImageView _iv = (ImageView) _mRightMenu
				.findViewById(R.id.iv_touch_howtouse);
		_iv.setOnClickListener(this);

		_iv = (ImageView) _mRightMenu.findViewById(R.id.iv_touch_feedback);
		_iv.setOnClickListener(this);

		_iv = (ImageView) _mRightMenu.findViewById(R.id.iv_touch_aboutus);
		_iv.setOnClickListener(this);
	}

	private void scaleView() {

		if (PRJFUNC.mGrp == null) {
			return;
		}
	}

	private void setBlankBufSize(BlankSize blankSize) {
		// double dpi = PRJFUNC.DPI / 160.0;
		RelativeLayout.LayoutParams lParams;
		lParams = (RelativeLayout.LayoutParams) m_ivBlankBuf.getLayoutParams();

		lParams.width = PRJFUNC.W_LCD;
		if (blankSize == BlankSize.None) {
			lParams.height = 1;
		} else {
			lParams.height = PRJFUNC.H_LCD;
		}
		m_ivBlankBuf.requestLayout();
	}

	// /////////////////////////////////////
	private void onBack() {
		int cur_page = _mScrollView.getViewIdx();
		if (cur_page == 0) {
			_mLeftListener.close();
		} else if (cur_page == 2) {
			_mRightListener.close();
		} else {
			finish();
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iv_blank_buffer:
			onBackgroundClick();
			break;
		case R.id.frm_detection_mode:
			onDetectionModeTouched();
			break;

		case R.id.frm_profile_home:
			onChangeProfileMode(PRJCONST.PROFILE_MODE_HOME);
			break;
		case R.id.frm_profile_office:
			onChangeProfileMode(PRJCONST.PROFILE_MODE_OFFICE);
			break;
		case R.id.frm_profile_drive:
			onChangeProfileMode(PRJCONST.PROFILE_MODE_DRIVE);
			break;
		case R.id.frm_profile_walk:
			onChangeProfileMode(PRJCONST.PROFILE_MODE_WALK);
			break;
		case R.id.iv_tutorial:
			SharedPreferencesMgr phoneDb = new SharedPreferencesMgr(mContext);
			phoneDb.setFirstHomeScreen();
			m_ivTutorial.setVisibility(View.GONE);
			break;
		case R.id.frm_pebble_watch:
			onMenuPebble(true);
			break;

		// Left Menu
		case R.id.frm_setup_home:
			onMenuSetupHome();
			break;
		case R.id.frm_profile_mode:
			onMenuProfileMode();
			break;
		case R.id.frm_paging_system:
			onMenuPagingSystem();
			break;
		case R.id.frm_pebble:
			onMenuPebble(false);
			break;
		case R.id.frm_advanced_settings:
			onMenuAdvancedSettings();
			break;
		case R.id.frm_exit:
			onMenuExit();
			break;

		// Right Menu
		case R.id.iv_touch_howtouse:
			onHelpHowtouse();
			break;
		case R.id.iv_touch_feedback:
			onHelpFeedback();
			break;
		case R.id.iv_touch_aboutus:
			onHelpAboutus();
			break;

		default:
			break;
		}
	}

	private void onChangeProfileMode(int p_nProfileMode) {

		if (PRJFUNC.setProfileMode(mContext, p_nProfileMode)) {
			if (GlobalValues.m_bProfileAutoSel) {
				GlobalValues.m_bProfileAutoSel = false;
				Toast.makeText(mContext,
						getString(R.string.profile_changed_to_manually),
						Toast.LENGTH_SHORT).show();

				SharedPreferencesMgr phoneDb = new SharedPreferencesMgr(
						mContext);
				phoneDb.saveProfileAutoSel();
			}
		}

		updateProfileModeUI();
	}

	private void onMenuSetupHome() {
		Intent intent = new Intent(HomeActivity.this, SetupActivity.class);
		startActivity(intent);
		overridePendingTransition(R.anim.right_in, R.anim.hold);
	}

	private void onMenuProfileMode() {
		Intent intent = new Intent(HomeActivity.this, ProfileModeActivity.class);
		startActivity(intent);
		overridePendingTransition(R.anim.right_in, R.anim.hold);
	}

	private void onMenuPagingSystem() {
		Intent intent = new Intent(HomeActivity.this,
				PagingSystemActivity.class);
		startActivity(intent);
		overridePendingTransition(R.anim.right_in, R.anim.hold);
	}

	private void onMenuPebble(boolean bDirectTest) {
		Intent intent = new Intent(HomeActivity.this, PebbleActivity.class);
		intent.putExtra(PebbleActivity.PARAM_DIRECT_TEST, bDirectTest);
		startActivity(intent);
		overridePendingTransition(R.anim.right_in, R.anim.hold);
	}

	private void onMenuAdvancedSettings() {
		Intent intent = new Intent(HomeActivity.this,
				AdvancedSettingsActivity.class);
		startActivity(intent);
		overridePendingTransition(R.anim.right_in, R.anim.hold);
	}

	private void onMenuExit() {
		Intent intent = new Intent(HomeActivity.this, DlgExit.class);
		startActivityForResult(intent, DLG_EXIT);
	}

	private void onRuninbackground() {
		finish();
	}

	private void onSwitchOff() {
		finish();

		SharedPreferencesMgr pPhoneDb = ((MainApplication) getApplication())
				.getSharedPreferencesMgrPoint();
		pPhoneDb.saveDetectionMode(false);

		if (GlobalValues._myService != null
				&& GlobalValues._myService.m_handler != null) {

			GlobalValues._myService.m_handler
					.sendEmptyMessage(GlobalValues.COMMAND_SYSTEM_EXIT);
		} else {
			System.exit(0);
		}
	}

	private void onLogout() {
		if (GlobalValues._soundEngine != null) {
			SoundEngine soundEngine = GlobalValues._soundEngine;
			if (soundEngine != null) {
				GlobalValues._soundEngine = null;
				soundEngine.Terminate();
			}
		}

		if (PRJCONST.IsInternetVersion) {
			ParseUser.logOut();
			ParsePush.unsubscribeInBackground(PRJFUNC
					.convertEmailToChannelStr(GlobalValues.m_stUserName));
		}

		GlobalValues.m_bDetect = false;
		SharedPreferencesMgr pPhoneDb = ((MainApplication) getApplication())
				.getSharedPreferencesMgrPoint();
		pPhoneDb.saveDetectionMode(GlobalValues.m_bDetect);

		if (GlobalValues._myService != null
				&& GlobalValues._myService.m_handler != null) {
			GlobalValues._myService.m_handler
					.sendEmptyMessage(GlobalValues.COMMAND_DISABLE_DETECT);
		}

		GlobalValues.m_stUserName = "";
		GlobalValues.m_stPassword = "";
		SharedPreferencesMgr phoneDb = new SharedPreferencesMgr(mContext);
		phoneDb.saveUserInfo(GlobalValues.m_stUserName,
				GlobalValues.m_stPassword);
		finish();

		Intent intent = new Intent(HomeActivity.this, _SplashActivity.class);
		startActivity(intent);
	}

	private void onHelpHowtouse() {
		// TODO Auto-generated method stub

	}

	private void onHelpFeedback() {
		Intent intent = new Intent(HomeActivity.this,
				HelpFeedbackActivity.class);
		startActivity(intent);
	}

	private void onHelpAboutus() {
		Intent intent = new Intent(HomeActivity.this, HelpAboutUsActivity.class);
		startActivity(intent);

	}

	private void onBackgroundClick() {
		int viewIdx = _mScrollView.getViewIdx();
		if (viewIdx == 0) {
			// left menu is opened.
			_mLeftListener.close();
		} else if (viewIdx == 2) {
			// right menu is opened.
			_mRightListener.close();
		} else {
			// main activity
		}
	}

	private void onDetectionModeTouched() {
		GlobalValues.m_bDetect = !GlobalValues.m_bDetect;
		if (GlobalValues.m_bDetect) {
			if (GlobalValues.m_bProfileIndoor) {
				boolean bRecorded = false;
				if (GlobalValues.recordedDetectData != null) {
					for (int i = 0; i < GlobalValues.recordedDetectData.length; i++) {
						ArrayList<DetectingData> detectData = PRJFUNC
								.getRecordingDataList(i);
						if (detectData != null && detectData.size() > 0) {
							bRecorded = true;
							break;
						}
					}
				}
				if (!bRecorded) {
					AlertDialog.Builder dlgAlert = new AlertDialog.Builder(
							mContext);

					dlgAlert.setMessage(getString(R.string.msg_no_recorded_sounds));
					dlgAlert.setTitle("Braci");
					dlgAlert.setPositiveButton("OK", null);
					dlgAlert.setCancelable(true);
					dlgAlert.create().show();
				}
			}
		}

		if (GlobalValues._soundEngine != null) {
			SoundEngine soundEngine = GlobalValues._soundEngine;
			if (soundEngine != null) {
				GlobalValues._soundEngine = null;
				soundEngine.Terminate();
			}
		}

		if (GlobalValues.m_bDetect) {
			GlobalValues._soundEngine = new SoundEngine();

			// if (GlobalValues._doorbellDetectData == null) {
			// GlobalValues._doorbellDetectData = new DetectingData();
			// GlobalValues._doorbellDetectData.LoadDetectData(SoundEngine.RECORD_DIR,
			// SoundEngine.DETECT_FILE);
			// }
		}

		udpateUIForDetectState();

		SharedPreferencesMgr pPhoneDb = ((MainApplication) getApplication())
				.getSharedPreferencesMgrPoint();
		pPhoneDb.saveDetectionMode(GlobalValues.m_bDetect);

		if (GlobalValues._myService != null
				&& GlobalValues._myService.m_handler != null) {
			if (GlobalValues.m_bDetect) {
				GlobalValues._myService.m_handler
						.sendEmptyMessage(GlobalValues.COMMAND_ENABLE_DETECT);
			} else {
				GlobalValues._myService.m_handler
						.sendEmptyMessage(GlobalValues.COMMAND_DISABLE_DETECT);
			}
		}

		m_equalizerView.invalidate();
	}

	private void updateProfileModeUI() {
		boolean bSelect = GlobalValues.m_bProfileIndoor
				&& GlobalValues.m_nProfileIndoorMode == PRJCONST.PROFILE_MODE_HOME;
		m_ivProfileHome.setSelected(bSelect);
		m_ivProfileHomeSel.setSelected(bSelect);
		if (bSelect) {
			m_tvProfileMode.setText(getString(R.string.indoor_home_title));
		}

		bSelect = GlobalValues.m_bProfileIndoor
				&& GlobalValues.m_nProfileIndoorMode == PRJCONST.PROFILE_MODE_OFFICE;
		m_ivProfileOffice.setSelected(bSelect);
		m_ivProfileOfficeSel.setSelected(bSelect);
		if (bSelect) {
			m_tvProfileMode.setText(getString(R.string.indoor_hotel_title));
		}

		bSelect = (!GlobalValues.m_bProfileIndoor)
				&& GlobalValues.m_nProfileOutdoorMode == PRJCONST.PROFILE_MODE_DRIVE;
		m_ivProfileDrive.setSelected(bSelect);
		m_ivProfileDriveSel.setSelected(bSelect);
		if (bSelect) {
			m_tvProfileMode.setText(getString(R.string.outdoor_drive_title));
		}

		bSelect = (!GlobalValues.m_bProfileIndoor)
				&& GlobalValues.m_nProfileOutdoorMode == PRJCONST.PROFILE_MODE_WALK;
		m_ivProfileWalk.setSelected(bSelect);
		m_ivProfileWalkSel.setSelected(bSelect);
		if (bSelect) {
			m_tvProfileMode.setText(getString(R.string.outdoor_bike_title));
		}
	}

	public Handler m_handlerProfile = new Handler() {
		public void handleMessage(android.os.Message msg) {
			updateProfileModeUI();
		}
	};
}
