package com.ugs.braci.settings;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.uc.prjcmn.PRJFUNC;
import com.uc.prjcmn.SharedPreferencesMgr;
import com.ugs.braci.GlobalValues;
import com.ugs.braci.R;
import com.ugs.braci.adapter.UIListAdapter;
import com.ugs.info.UIListItemInfo;

public class AlertingFragment extends Fragment implements OnClickListener {

	private AdvancedSettingsActivity mActivity;

	private ListView m_listItems;
	private UIListAdapter m_adapterItems;

	private TextView m_tvDescription;

	private Fragment m_oldFragment;
	private String m_strOldTitle;

	private final int ITEM_ALERT_VIBRATION = 0;
	private final int ITEM_ALERT_FLASHLIGHT = 1;
	private final int ITEM_ALERT_PEBBLEWATCH = 2;
	private final int ITEM_ALERT_ANDROIDWEAR = 3;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		final View v = inflater.inflate(R.layout.fragment_general, container,
				false);

		mActivity = (AdvancedSettingsActivity) getActivity();

		m_oldFragment = mActivity.m_curFragment;
		m_strOldTitle = mActivity.m_tvTitle.getText().toString();

		mActivity.m_curFragment = AlertingFragment.this;
		mActivity.m_tvTitle.setText(mActivity
				.getString(R.string.setting_alert_title));

		if (m_oldFragment == null) {
			PRJFUNC.hideFragment(mActivity, mActivity.m_settingsFragment);
		} else {
			PRJFUNC.hideFragment(mActivity, m_oldFragment);
		}

		updateLCD(v);

		// - update position
		if (!PRJFUNC.DEFAULT_SCREEN) {

		}

		return v;
	}

	@Override
	public void onDestroyView() {
		if (mActivity.m_curFragment == AlertingFragment.this) {
			mActivity.m_curFragment = m_oldFragment;
			mActivity.m_tvTitle.setText(m_strOldTitle);

			if (mActivity.m_curFragment == null) {
				PRJFUNC.showFragment(mActivity, mActivity.m_settingsFragment);
			} else {
				PRJFUNC.showFragment(mActivity, m_oldFragment);
			}
		}
		
		SharedPreferencesMgr phoneDb = new SharedPreferencesMgr(mActivity);
		phoneDb.saveDetectedActInfo();

		super.onDestroyView();
	}

	public void refreshList() {
		m_adapterItems.clear();

		UIListItemInfo itemInfo = null;

		itemInfo = new UIListItemInfo(R.drawable.ic_alerting_vibration,
				mActivity.getString(R.string.alert_vibration_title),
				mActivity.getString(R.string.alert_vibration_desc), true);
		itemInfo.setChecked(GlobalValues.m_bNotifyVibrate);
		m_adapterItems.add(itemInfo);

		itemInfo = new UIListItemInfo(R.drawable.ic_alerting_flash,
				mActivity.getString(R.string.alert_flashlight_title),
				mActivity.getString(R.string.alert_flashlight_desc), true);
		itemInfo.setChecked(GlobalValues.m_bNotifyFlash);
		m_adapterItems.add(itemInfo);

		itemInfo = new UIListItemInfo(R.drawable.ic_alerting_pebble,
				mActivity.getString(R.string.alert_pebble_title),
				mActivity.getString(R.string.alert_pebble_desc), true);
		itemInfo.setChecked(GlobalValues.m_bNotifyPebbleWatch);
		m_adapterItems.add(itemInfo);

		itemInfo = new UIListItemInfo(R.drawable.ic_alerting_androidwear,
				mActivity.getString(R.string.alert_androidwear_title),
				mActivity.getString(R.string.alert_androidwear_desc), true);
		itemInfo.setChecked(GlobalValues.m_bNotifyAndroidWear);
		m_adapterItems.add(itemInfo);
	}

	// //////////////////////////////////////////////////
	private void updateLCD(View v) {

		if (PRJFUNC.mGrp == null) {
			PRJFUNC.resetGraphValue(mActivity);
		}

		m_tvDescription = (TextView) v.findViewById(R.id.tv_desc);
		m_tvDescription
				.setText(mActivity.getString(R.string.alert_method_desc));

		m_listItems = (ListView) v.findViewById(R.id.listview);
		m_adapterItems = new UIListAdapter(mActivity,
				R.layout.list_item_title_and_desc, new ArrayList<UIListItemInfo>());
		m_adapterItems.setCallback(m_adapterCallback);
		m_listItems.setAdapter(m_adapterItems);

		View _v = v.findViewById(R.id.frm_btn_ok);
		_v.setVisibility(View.VISIBLE);

		ImageView _iv = (ImageView) v.findViewById(R.id.iv_ok);
		_iv.setOnClickListener(this);

		refreshList();
	}

	UIListAdapter.Callback m_adapterCallback = new UIListAdapter.Callback() {
		@Override
		public void onItemClick(int p_nPosition) {
			UIListItemInfo itemInfo = m_adapterItems.getItem(p_nPosition);
			itemInfo.m_bChecked = !itemInfo.m_bChecked;
			m_adapterItems.notifyDataSetChanged();

			switch (p_nPosition) {
			case ITEM_ALERT_VIBRATION:
				GlobalValues.m_bNotifyVibrate = itemInfo.m_bChecked;
				break;
			case ITEM_ALERT_FLASHLIGHT:
				GlobalValues.m_bNotifyFlash = itemInfo.m_bChecked;
				break;
			case ITEM_ALERT_PEBBLEWATCH:
				GlobalValues.m_bNotifyPebbleWatch = itemInfo.m_bChecked;
				break;
			case ITEM_ALERT_ANDROIDWEAR:
				GlobalValues.m_bNotifyAndroidWear = itemInfo.m_bChecked;
				break;
			default:
				break;
			}
		}
	};

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iv_ok:
			mActivity.goBack();
			break;
		default:
			break;
		}

	}
}
