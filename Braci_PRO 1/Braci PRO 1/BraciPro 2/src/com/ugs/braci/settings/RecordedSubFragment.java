package com.ugs.braci.settings;

import java.io.File;
import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.ugs.braci.GlobalValues;
import com.ugs.braci.R;
import com.ugs.braci.adapter.UIRecordedListAdapter;
import com.ugs.braci.engine.DetectingData;
import com.ugs.braci.engine.SoundEngine;
import com.ugs.braci.setup.SetupActivity;
import com.ugs.info.UIListItemInfo;

public class RecordedSubFragment extends Fragment implements OnClickListener {

	private AdvancedSettingsActivity mActivity;

	private ListView m_listItems;
	private UIRecordedListAdapter m_adapterItems;

	private TextView m_tvDescription;

	private Fragment m_oldFragment;
	private String m_strOldTitle;

	private int m_nProfileMode;

	public void setProfileMode(int p_nProfileMode) {
		m_nProfileMode = p_nProfileMode;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		final View v = inflater.inflate(R.layout.fragment_general, container,
				false);

		mActivity = (AdvancedSettingsActivity) getActivity();

		m_oldFragment = mActivity.m_curFragment;
		m_strOldTitle = mActivity.m_tvTitle.getText().toString();

		mActivity.m_curFragment = RecordedSubFragment.this;

		int[] strTitleIds = { R.string.recorded_home_title,
				R.string.recorded_office_title, R.string.recorded_drive_title,
				R.string.recorded_bike_title, };

		mActivity.m_tvTitle.setText(mActivity
				.getString(strTitleIds[m_nProfileMode]));

		if (m_oldFragment == null) {
			PRJFUNC.hideFragment(mActivity, mActivity.m_settingsFragment);
		} else {
			PRJFUNC.hideFragment(mActivity, m_oldFragment);
		}

		updateLCD(v);

		// - update position
		if (!PRJFUNC.DEFAULT_SCREEN) {

		}

		return v;
	}

	@Override
	public void onDestroyView() {
		if (mActivity.m_curFragment == RecordedSubFragment.this) {
			mActivity.m_curFragment = m_oldFragment;
			mActivity.m_tvTitle.setText(m_strOldTitle);

			if (mActivity.m_curFragment == null) {
				PRJFUNC.showFragment(mActivity, mActivity.m_settingsFragment);
			} else {
				PRJFUNC.showFragment(mActivity, m_oldFragment);
			}
		}

		super.onDestroyView();
	}

	private int[] m_imgIndoorSoundIds = { R.drawable.ic_setup_doorbell, R.drawable.ic_setup_doorbell,
			R.drawable.ic_setup_smokealarm, R.drawable.ic_setup_landline,
			R.drawable.ic_setup_intercom, R.drawable.ic_setup_carbon,
			R.drawable.ic_setup_alarmclock, R.drawable.ic_setup_thief,
			R.drawable.ic_setup_microwave };

	private int[] m_nIndoorSoundTitleIds = { R.string.setup_doorbell_title, R.string.setup_backdoorbell_title,
			R.string.setup_smokealarm_title, R.string.setup_landline_title,
			R.string.setup_intercom_title, R.string.setup_carbon_title,
			R.string.setup_alarmclock_title, R.string.setup_thief_title,
			R.string.setup_microwave_title };

	private int[] m_imgOutdoorSoundIds = { R.drawable.ic_recorded_carhorn,
			R.drawable.ic_recorded_traffic, R.drawable.ic_recorded_police,
			R.drawable.ic_recorded_ambulance, R.drawable.ic_recorded_train };

	private int[] m_nOutdoorSoundTitleIds = { R.string.recorded_outdoor_car,
			R.string.recorded_outdoor_traffic,
			R.string.recorded_outdoor_police,
			R.string.recorded_outdoor_ambulance,
			R.string.recorded_outdoor_train, };

	public void refreshList() {
		m_adapterItems.clear();

		UIListItemInfo itemInfo = null;

		if (m_nProfileMode == PRJCONST.PROFILE_MODE_HOME
				|| m_nProfileMode == PRJCONST.PROFILE_MODE_OFFICE) {

			for (int type = 0; type < PRJCONST.REC_SOUND_TYPE_CNT; type++) {
				if (SetupActivity.m_bSoundsRecordable[type]) {
					if (isExistSound(type)) {
						itemInfo = new UIListItemInfo(
								m_imgIndoorSoundIds[type],
								mActivity
										.getString(m_nIndoorSoundTitleIds[type]),
								null, true);
						itemInfo.setChecked(false);
						itemInfo.m_extraData = (Integer) type;
						m_adapterItems.add(itemInfo);
					}
				} else {
					itemInfo = new UIListItemInfo(m_imgIndoorSoundIds[type],
							mActivity.getString(m_nIndoorSoundTitleIds[type]),
							null, false);
					m_adapterItems.add(itemInfo);
				}
			}
		} else {
			for (int type = 0; type < m_nOutdoorSoundTitleIds.length; type++) {
				itemInfo = new UIListItemInfo(m_imgOutdoorSoundIds[type],
						mActivity.getString(m_nOutdoorSoundTitleIds[type]),
						null, false);
				itemInfo.setChecked(false);
				m_adapterItems.add(itemInfo);
			}
		}
	}

	private boolean isExistSound(int p_nRecSndType) {
		if (p_nRecSndType == PRJCONST.REC_SOUND_TYPE_THEFTALARM) {
			if (SoundEngine.THIEF_ALARM_IDX >= 0) 
				return true;
			else 
				return false;
		}
		String strFilePath = PRJFUNC
				.getRecordDir(p_nRecSndType, m_nProfileMode) + "/Detect.dat";
		File file = new File(strFilePath);
		if (file.exists())
			return true;
		return false;
	}

	// //////////////////////////////////////////////////
	private void updateLCD(View v) {

		if (PRJFUNC.mGrp == null) {
			PRJFUNC.resetGraphValue(mActivity);
		}

		m_tvDescription = (TextView) v.findViewById(R.id.tv_desc);
		m_tvDescription.setText(mActivity
				.getString(R.string.setting_recorde_screen_desc));

		m_listItems = (ListView) v.findViewById(R.id.listview);
		m_adapterItems = new UIRecordedListAdapter(mActivity,
				R.layout.list_item_recorded, new ArrayList<UIListItemInfo>());
		m_adapterItems.setCallback(m_adapterCallback);
		m_listItems.setAdapter(m_adapterItems);

		// View _v = v.findViewById(R.id.frm_btn_ok);
		// _v.setVisibility(View.VISIBLE);

		ImageView _iv = (ImageView) v.findViewById(R.id.iv_ok);
		_iv.setOnClickListener(this);

		refreshList();
	}

	UIRecordedListAdapter.Callback m_adapterCallback = new UIRecordedListAdapter.Callback() {
		@Override
		public void onItemClick(int p_nPosition) {
			if (m_nProfileMode != PRJCONST.PROFILE_MODE_HOME
					&& m_nProfileMode != PRJCONST.PROFILE_MODE_OFFICE)
				return;

			final UIListItemInfo item = m_adapterItems.getItem(p_nPosition);
			if (item.m_bCheckable) {
				AlertDialog.Builder alertDialog = new AlertDialog.Builder(
						mActivity);
				alertDialog.setTitle(R.string.setting_recorded_title);
				alertDialog.setMessage(R.string.recorded_delete_sound);
				alertDialog.setPositiveButton(R.string.word_yes,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								int nSndType = (Integer) item.m_extraData;
								PRJFUNC.deleteFile(new File(PRJFUNC
										.getRecordDir(nSndType, m_nProfileMode)), false);
								if (m_nProfileMode == GlobalValues.m_nProfileIndoorMode) {
									ArrayList<DetectingData> detectingDataList = PRJFUNC
											.getRecordingDataList(nSndType);
									if (detectingDataList != null)
										detectingDataList.clear();
								}
								m_adapterItems.remove(item);
							}
						});
				alertDialog.setNegativeButton(R.string.word_no, null);
				alertDialog.show();
			}
		}
	};

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iv_ok:
			mActivity.goBack();
			break;

		default:
			break;
		}
	}

}
