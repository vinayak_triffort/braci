package com.ugs.braci.settings;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.uc.prjcmn.ActivityTask;
import com.uc.prjcmn.PRJFUNC;
import com.ugs.braci.R;

public class AdvancedSettingsActivity extends FragmentActivity implements
		OnClickListener {

	private Context mContext;

	public AdvancedSettingsFragment m_settingsFragment;
	public Fragment m_curFragment;

	public TextView m_tvTitle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setup);
		ActivityTask.INSTANCE.add(this);

		mContext = this;

		initValues();

		updateLCD();

		m_tvTitle.setText(getString(R.string.menu_advanced_settings));

		m_settingsFragment = new AdvancedSettingsFragment();
		PRJFUNC.addFragment(AdvancedSettingsActivity.this, m_settingsFragment);

		m_curFragment = null;
	}

	private void initValues() {

	}

	@Override
	protected void onDestroy() {
		PRJFUNC.closeProgress(mContext);

		ActivityTask.INSTANCE.remove(this);

		releaseValues();

		super.onDestroy();
	}

	private void releaseValues() {
		m_settingsFragment = null;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			goBack();
			return false;
		}

		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	private void updateLCD() {
		m_tvTitle = (TextView) findViewById(R.id.tv_title);
		// PRJFUNC.setTextViewFont(mContext, m_tvTitle,
		// PRJCONST.FONT_AuctionGothicBold);

		ImageView _iv = (ImageView) findViewById(R.id.iv_back);
		_iv.setOnClickListener(this);

		_iv = (ImageView) findViewById(R.id.iv_help_menu);
		_iv.setOnClickListener(this);
		_iv.setVisibility(View.GONE);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iv_back:
			goBack();
			break;
		case R.id.iv_help_menu:
			onHelpMenu();
			break;
		default:
			break;
		}
	}

	private void onHelpMenu() {
		// TODO Auto-generated method stub

	}

	public void goBack() {
		if (m_curFragment != null) {
			PRJFUNC.removeFragment(AdvancedSettingsActivity.this, m_curFragment);
		} else {
			finish();
			overridePendingTransition(R.anim.hold, R.anim.right_out);
		}
	}

	public void onRecordedSounds() {
		RecordedFragment fragment = new RecordedFragment();
		PRJFUNC.addFragment(AdvancedSettingsActivity.this, fragment);		
	}
	
	public void onSubRecordedSounds(int p_nProfileMode) {
		RecordedSubFragment fragment = new RecordedSubFragment();
		fragment.setProfileMode(p_nProfileMode);
		PRJFUNC.addFragment(AdvancedSettingsActivity.this, fragment);		
	}
	
	public void onAlertingMethodSelected() {
		AlertingFragment fragment = new AlertingFragment();
		PRJFUNC.addFragment(AdvancedSettingsActivity.this, fragment);
	}
	
	public void onRepairMethodSelected() {
		RepairFragment fragment = new RepairFragment();
		PRJFUNC.addFragment(AdvancedSettingsActivity.this, fragment);		
	}

	public void onRepairProfileSelected(int p_nFixType) {
		RepairProfileFragment fragment = new RepairProfileFragment();
		fragment.setFixType(p_nFixType);
		PRJFUNC.addFragment(AdvancedSettingsActivity.this, fragment);		
	}
	
	public void onRepairSubSelected(int p_nFixType, int p_nSoundType) {
		RepairSubFragment fragment = new RepairSubFragment();
		fragment.setFixType(p_nFixType);
		fragment.setProfileMode(p_nSoundType);
		PRJFUNC.addFragment(AdvancedSettingsActivity.this, fragment);		
	}
}
