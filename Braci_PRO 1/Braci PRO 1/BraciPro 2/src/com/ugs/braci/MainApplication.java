package com.ugs.braci;

import java.io.File;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.parse.Parse;
import com.parse.PushService;
import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.uc.prjcmn.SharedPreferencesMgr;
import com.ugs.braci.engine.SoundEngine;

public class MainApplication extends Application {

	public static Context m_Context = null;
	private SharedPreferencesMgr m_pPhoneDb = null;

	@Override
	public void onCreate() {

		super.onCreate();

		m_Context = getApplicationContext();

		if (true) {
			File file = new File(SoundEngine.RECORD_DIR);
			if (file.exists())
				PRJFUNC.deleteFile(file, false);

			SoundEngine.RECORD_DIR = this.getFilesDir().getAbsolutePath()
					+ "/Data";
		}

		// ///////////////// ///////////
		m_pPhoneDb = new SharedPreferencesMgr(this);
		long first_time = m_pPhoneDb.getFirstRunTime();
		if (first_time == 0) {
			GlobalValues._firstRun = true;
		}

		setSenitivityForDevice();

		m_pPhoneDb.loadDetectedActInfo();
		m_pPhoneDb.loadHomeLoacation();
		m_pPhoneDb.loadOfficeLoacation();
		m_pPhoneDb.loadProfileInfo();
		m_pPhoneDb.loadRecordedDetectableInfo();

		m_pPhoneDb.loadUnivEngThresholds();

		PRJFUNC.setBabyCryingThreshold(this);

		// if (PRJCONST.IsInternetVersion) {
		Parse.initialize(this, "2sbHYms6RStUnI8JzOFiirpRn2aW9KhiKQ4h6QQZ",
				"LUn4jFag12l96AgD4guHKIey5RwTGogYyxne5VRN");
		PushService.setDefaultPushCallback(this, _SplashActivity.class);
		// }

		startService();
	}

	private void setSenitivityForDevice() {
		String strManufacture = Build.MANUFACTURER;
		strManufacture = strManufacture.toLowerCase();
		if (strManufacture.equals("samsung")) {
			GlobalValues.DEVICE_SENSITIVITY = GlobalValues.SENSITIVITY_SAMSUNG;
		} else if (strManufacture.equals("lge")) {
			GlobalValues.DEVICE_SENSITIVITY = GlobalValues.SENSITIVITY_LGE;
		} else if (strManufacture.equals("htc")) {
			GlobalValues.DEVICE_SENSITIVITY = GlobalValues.SENSITIVITY_HTC;
		}
	}

	@Override
	public void onTerminate() {

		m_Context = null;
		super.onTerminate();
	}

	public void startService() {
		if (!PRJCONST.IsTestVersion) {
			Intent intent = new Intent("com.ugs.braciproservice");

			startService(intent);
		}
	}

	public SharedPreferencesMgr getSharedPreferencesMgrPoint() {
		return m_pPhoneDb;
	}

}
