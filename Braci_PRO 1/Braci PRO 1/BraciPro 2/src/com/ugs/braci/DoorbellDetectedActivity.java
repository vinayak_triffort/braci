package com.ugs.braci;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.getpebble.android.kit.PebbleKit;
import com.getpebble.android.kit.util.PebbleDictionary;
import com.uc.prjcmn.ActivityTask;
import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.uc.prjcmn.PRJFUNC.Signal;
import com.uc.prjcmn.SharedPreferencesMgr;
import com.ugs.braci.engine.SoundEngine;
import com.ugs.braci.R;

public class DoorbellDetectedActivity extends Activity implements
		OnClickListener {

	private final String TAG = "_DoorbellDetectedActivity";

	public static final String EXTRA_PARAM = "EXTRA_PARAM";
	public static final int TOTAL_SOUND_TIME = 10000; // ms
	public static final int TOTAL_ON_TIME = 120000; // ms
	public static final long VIBRATE_DURATION = 700L;

	private long m_nVibrationDuration = VIBRATE_DURATION;
	private long m_nVibrationSpace = 300L;

	long[][] VIBRATION_PATTERNS = { { 700L, 400L }, // FIRE_ALARM 0
			{ 300L, 300L }, // DOOR_BELL 1
			{ 700L, 300L }, // BABY_CRYING 2
			{ 700L, 300L }, // MOBILE_RINGING 3
			{ 700L, 300L }, // CAR_HORN 4
			{ 700L, 300L }, // THIEF_ALARM 5
			{ 700L, 300L }, // ALARM_CLOCK 6
			{ 700L, 300L }, // WAKE_UP 7
			{ 700L, 300L }, // BED_TIME 8
			{ 700L, 300L }, // EAT_TIME 9
			{ 700L, 300L }, // SOS 10
			{ 700L, 300L }, // NO 11
			{ 700L, 300L }, // YES 12
			{ 300L, 300L }, // CALLING_YOU 13
			{ 700L, 300L }, // TRAFFIC_TONE 14
			{ 700L, 300L }, // TRAIN_WHISTLE 15
			{ 700L, 300L }, // MICROWAVE 16
			{ 700L, 300L }, // POLICE_SIREN 17
			{ 700L, 300L }, // INTERCOM 18
			{ 700L, 300L }, // DOG_PARK 19
			{ 700L, 300L }, // SNORE 20
			{ 700L, 300L }, // SMOKE_ALARM 21
			{ 700L, 300L }, // CO2 22
	};

	private PebbleKit.PebbleDataReceiver[] myDataHandler = null;
	Handler m_handlerTerminate;

	private Context mContext;
	private boolean m_bRinging;

	private ImageView m_ivCenter;

	private Camera mCamera = null;

	private PRJFUNC.Signal m_signal = PRJFUNC.Signal.NONE;

	private int m_nStep = 0;

	private int m_nOrgStreamVolume = 0;

	// ////////////////////////////////////////////////////////////
	// //////////////////////////////////////////////////////////////
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_doorbell_detected);
		ActivityTask.INSTANCE.add(this);

		getWindow().addFlags(
				WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
						| WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
						| WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
						| WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

		mContext = (Context) this;

		myDataHandler = null;

		Intent intent = getIntent();
		int nSignal = intent.getIntExtra(EXTRA_PARAM, -1);
		m_signal = PRJFUNC.ConvIntToSignal(nSignal);

		// set vibration length
		if (nSignal >= 0 && nSignal < VIBRATION_PATTERNS.length) {
			m_nVibrationDuration = VIBRATION_PATTERNS[nSignal][0];
			m_nVibrationSpace = VIBRATION_PATTERNS[nSignal][1];
		}

		updateLCD();

		GlobalValues._bEnabledActivity = true;

		// - update position
		if (!PRJFUNC.DEFAULT_SCREEN) {
			scaleView();
		}

		startAlarm();

		// // if alarm
		if (m_signal == PRJFUNC.Signal.ALARM_CLOCK) {
			SharedPreferencesMgr pPhoneDb = ((MainApplication) getApplication())
					.getSharedPreferencesMgrPoint();
			pPhoneDb.saveAlarmTime(0);
		}

		final AudioManager audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
		m_nOrgStreamVolume = audioManager
				.getStreamVolume(AudioManager.STREAM_MUSIC);
		audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);

		m_handlerTerminate = new Handler();
		m_handlerTerminate.postDelayed(new Runnable() {
			@Override
			public void run() {
				goMainActivity();
			}
		}, TOTAL_ON_TIME);

		// PushWakeLock.releaseCpuLock();
	}

	@Override
	protected void onResume() {
		super.onResume();

		if (myDataHandler == null) {

			myDataHandler = new PebbleKit.PebbleDataReceiver[PRJFUNC.NOTIFY_UUIDs.length];

			for (int i = 0; i < PRJFUNC.NOTIFY_UUIDs.length; i++) {

				myDataHandler[i] = new PebbleKit.PebbleDataReceiver(
						PRJFUNC.NOTIFY_UUIDs[i]) {

					@Override
					public void receiveData(Context context, int transactionId,
							PebbleDictionary data) {

						stopAlarm();
						goMainActivity();

						// PRJFUNC.sendSignalToPebble(GlobalValues._myService,
						// Signal.STOP);
						// PRJFUNC.closeAppOnPebble(GlobalValues._myService);
						{
							NotificationManager mNotificationManager;
							String ns = Context.NOTIFICATION_SERVICE;
							mNotificationManager = (NotificationManager) getSystemService(ns);
							mNotificationManager.cancelAll();

							GlobalValues.m_bDetectionPhone = false;
						}
					}
				};
				PebbleKit.registerReceivedDataHandler(getApplicationContext(),
						myDataHandler[i]);
			}
		}
	}

	@Override
	protected void onDestroy() {
		releaseValues();

		if (m_bRinging)
			stopAlarm();

		getWindow().clearFlags(
				WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
						| WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
						| WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
						| WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

		final AudioManager audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
		audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
				m_nOrgStreamVolume, 0);

		ActivityTask.INSTANCE.remove(this);
		super.onDestroy();

		GlobalValues._bEnabledActivity = false;

		m_handlerTerminate = null;
	}

	private void releaseValues() {

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			onCloseActivity();
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}

	void onCloseActivity() {
		if (m_bRinging) {
			stopAlarm();

			PRJFUNC.sendSignalToPebble(GlobalValues._myService, Signal.STOP);
			PRJFUNC.closeAppOnPebble(GlobalValues._myService);
		}

		PRJFUNC.sendSignalToPebble(GlobalValues._myService, Signal.STOP);
		PRJFUNC.closeAppOnPebble(GlobalValues._myService);

		if (GlobalValues._myService != null
				&& GlobalValues._myService.m_handler != null) {
			GlobalValues._myService.m_handler
					.sendEmptyMessage(GlobalValues.COMMAND_REMOVE_NOTIFY);
		}

		{
			NotificationManager mNotificationManager;
			String ns = Context.NOTIFICATION_SERVICE;
			mNotificationManager = (NotificationManager) getSystemService(ns);
			mNotificationManager.cancelAll();

			GlobalValues.m_bDetectionPhone = false;
		}

		goMainActivity();
	}

	String convertToBinaryString(int nValue, int nDigitsNum) {
		String str = Integer.toBinaryString(nValue);
		if (str.length() > nDigitsNum)
			return str;

		while (nDigitsNum > str.length()) {
			str = "0" + str;
		}
		return str;
	}

	// //////////////////////////////////////////////////
	private void updateLCD() {

		if (PRJFUNC.mGrp == null) {
			PRJFUNC.resetGraphValue(mContext);
		}

		m_ivCenter = (ImageView) findViewById(R.id.iv_center);
		TextView _tvTitle = (TextView) findViewById(R.id.tv_title);
		PRJFUNC.setTextViewFont(mContext, _tvTitle, PRJCONST.FONT_OpenSansBold);

		ImageView _iv = (ImageView) findViewById(R.id.iv_confirm);
		_iv.setOnClickListener(this);

		_iv = (ImageView) findViewById(R.id.iv_false);
		_iv.setOnClickListener(this);

		String strText = "";
		if (m_signal == PRJFUNC.Signal.DOOR_BELL) {
			strText = (getResources()
					.getString(R.string.doorbell_detected_title));
			m_ivCenter.setImageResource(R.drawable.center_detected);
		} else if (m_signal == PRJFUNC.Signal.BACKDOOR_BELL) {
			strText = (getResources()
					.getString(R.string.backdoorbell_detected_title));
			m_ivCenter.setImageResource(R.drawable.center_detected);

		} else if (m_signal == PRJFUNC.Signal.FIRE_ALARM
				|| m_signal == PRJFUNC.Signal.CO2) {
			strText = (getResources().getString(R.string.fire_detected_title));
			m_ivCenter.setImageResource(R.drawable.center_detected_fire);
		} else if (m_signal == PRJFUNC.Signal.SMOKE_ALARM) {
			strText = (getResources().getString(R.string.smoke_detected_title));
			m_ivCenter.setImageResource(R.drawable.center_detected_fire);
		} else if (m_signal == PRJFUNC.Signal.BABY_CRYING) {
			strText = (getResources()
					.getString(R.string.babycrying_detected_title));
			m_ivCenter.setImageResource(R.drawable.center_detected_baby);
		} else if (m_signal == PRJFUNC.Signal.MOBILE_RINGING) {
			strText = (getResources().getString(R.string.mobile_detected_title));
			m_ivCenter.setImageResource(R.drawable.center_detected_phonering);
		} else if (m_signal == PRJFUNC.Signal.CAR_HORN) {
			strText = (getResources()
					.getString(R.string.carhorns_detected_title));
			m_ivCenter.setImageResource(R.drawable.center_detected_carhorn);
		} else if (m_signal == PRJFUNC.Signal.THIEF_ALARM) {
			strText = (getResources().getString(R.string.thief_detected_title));
			m_ivCenter.setImageResource(R.drawable.center_detected_thief);
		} else if (m_signal == PRJFUNC.Signal.ALARM_CLOCK) {
			strText = (getResources().getString(R.string.alarm_detected_title));
			m_ivCenter.setImageResource(R.drawable.center_detected_alarm);
		} else if (m_signal == PRJFUNC.Signal.WAKE_UP) {
			strText = (getResources().getString(R.string.paging_wakeup_title));
			m_ivCenter.setImageResource(R.drawable.center_paging_wakeup);
		} else if (m_signal == PRJFUNC.Signal.BED_TIME) {
			strText = (getResources().getString(R.string.paging_bed_title));
			m_ivCenter.setImageResource(R.drawable.center_paging_bedtime);
		} else if (m_signal == PRJFUNC.Signal.SOS) {
			strText = (getResources()
					.getString(R.string.paging_sos_receive_title));
			m_ivCenter.setImageResource(R.drawable.center_paging_sos);
		} else if (m_signal == PRJFUNC.Signal.MEAL_TIME) {
			strText = (getResources().getString(R.string.paging_meal_title));
			m_ivCenter.setImageResource(R.drawable.center_paging_eattime);
		} else if (m_signal == PRJFUNC.Signal.NO) {
			strText = (getResources().getString(R.string.paging_no_title));
			m_ivCenter.setImageResource(R.drawable.center_paging_no);
		} else if (m_signal == PRJFUNC.Signal.YES) {
			strText = (getResources().getString(R.string.paging_yes_title));
			m_ivCenter.setImageResource(R.drawable.center_paging_yes);
		} else if (m_signal == PRJFUNC.Signal.CALLING_YOU) {
			strText = (getResources()
					.getString(R.string.paging_call_receive_title));
			m_ivCenter.setImageResource(R.drawable.center_paging_calling);
		} else if (m_signal == PRJFUNC.Signal.INTERCOM) {
			strText = (getResources()
					.getString(R.string.intercom_detected_title));
			m_ivCenter.setImageResource(R.drawable.center_detected_intercom);
		} else if (m_signal == PRJFUNC.Signal.DOG_PARK) {
			strText = (getResources()
					.getString(R.string.dogbark_detected_title));
			m_ivCenter.setImageResource(R.drawable.center_detected_dogpark);
		} else if (m_signal == PRJFUNC.Signal.MICROWAVE) {
			strText = (getResources()
					.getString(R.string.microwave_detected_title));
			m_ivCenter.setImageResource(R.drawable.center_detected_microwave);
		} else if (m_signal == PRJFUNC.Signal.TRAFFIC_TONE) {
			strText = (getResources()
					.getString(R.string.traffictone_detected_title));
			m_ivCenter.setImageResource(R.drawable.center_traffic_tone);
		} else if (m_signal == PRJFUNC.Signal.TRAIN_WHISTLE) {
			strText = (getResources().getString(R.string.train_detected_title));
			m_ivCenter.setImageResource(R.drawable.center_train_whistle);
		} else if (m_signal == PRJFUNC.Signal.POLICE_SIREN) {
			strText = (getResources().getString(R.string.police_detected_title));
			m_ivCenter.setImageResource(R.drawable.center_detected_police);
		} else if (m_signal == PRJFUNC.Signal.SNORE) {
			strText = (getResources().getString(R.string.snore_detected_title));
			m_ivCenter.setImageResource(R.drawable.center_detected_snoring);
		}

		if (PRJFUNC.DETECTED_NUMBER > 0) {
			strText = strText + " " + PRJFUNC.DETECTED_NUMBER;
		}
		_tvTitle.setText(strText);
	}

	private void scaleView() {

		if (PRJFUNC.mGrp == null) {
			return;
		}
	}

	// /////////////////////////////////////
	private void goMainActivity() {
		// if (!GlobalValues._bExistHomeActivity) {
		// Intent it = new Intent(DoorbellDetectedActivity.this,
		// HomeActivity.class);
		// startActivity(it);
		// finish();
		// overridePendingTransition(R.anim.left_in, R.anim.hold);
		// return;
		// }
		finish();
		overridePendingTransition(R.anim.hold, R.anim.right_out);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iv_confirm:
			onCloseActivity();
			break;

		case R.id.iv_false:
			onFalseActivity();
			break;

		default:
			break;
		}
	}

	private void onFalseActivity() {
		fixSound(m_signal);
		onCloseActivity();
	}

	private void startAlarm() {
		if (GlobalValues._myService != null) {
		}

		m_nStep = 0;
		m_bRinging = true;
		// Camera Flash
		if (GlobalValues.m_bNotifyFlash) {
			if (mCamera == null) {
				mCamera = Camera.open();
			}
			flashing();
		}

		// Vibration
		if (GlobalValues.m_bNotifyVibrate) {
			vibrating(true);
		}
	}

	private void stopAlarm() {
		if (mCamera != null) {
			mCamera.release();
			mCamera = null;
		}

		Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		vibrator.cancel();

		m_bRinging = false;
	}

	private void vibrating(boolean immediately) {
		if (!m_bRinging)
			return;

		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				if (!m_bRinging)
					return;

				Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
				vibrator.vibrate(m_nVibrationDuration);

				vibrating(false);
			}
		}, immediately ? 100 : m_nVibrationDuration + m_nVibrationSpace);
	}

	private void flashing() {
		m_nStep++;

		if (m_nStep * SoundEngine.FLASH_DURATION > TOTAL_SOUND_TIME) {
			stopAlarm();

			PRJFUNC.sendSignalToPebble(GlobalValues._myService, Signal.STOP);
			PRJFUNC.closeAppOnPebble(GlobalValues._myService);
		}

		if (!m_bRinging)
			return;

		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				if (!m_bRinging)
					return;

				if (mCamera != null) {
					Camera.Parameters mParameters = mCamera.getParameters();
					if (m_nStep % 2 == 1) {
						mParameters.setFlashMode(Parameters.FLASH_MODE_TORCH);
						mCamera.setParameters(mParameters);
					} else {
						mParameters.setFlashMode(Parameters.FLASH_MODE_OFF);
						mCamera.setParameters(mParameters);
					}
					flashing();
				}
			}
		}, SoundEngine.FLASH_DURATION);
	}

	private void fixSound(Signal p_Signal) {
		// fixing about false alarms
		SharedPreferencesMgr phoneDb = new SharedPreferencesMgr(mContext);
		if (p_Signal == Signal.SMOKE_ALARM || p_Signal == Signal.CO2) {
			int nUnivEngineType = convertRecTypeToUniEng(p_Signal);
			SoundEngine.UNIVERSAL_THRESHOLDS[nUnivEngineType] += 0.5f;
			phoneDb.saveUnivEngThreshold(nUnivEngineType);
		} else {

			int nSignal = PRJFUNC.ConvSignalToInt(p_Signal);
			SoundEngine.DELTA_MATCH_RATES[nSignal] += 0.02f;
			phoneDb.saveMatchingRateDelta(PRJFUNC.getCurProfileMode(), nSignal,
					SoundEngine.DELTA_MATCH_RATES[nSignal]);
		}
	}

	private int convertRecTypeToUniEng(Signal p_signal) {
		if (p_signal == Signal.SMOKE_ALARM)
			return PRJCONST.UNIVERSAL_ENGINE_SMOKEALARM;
		if (p_signal == Signal.CO2)
			return PRJCONST.UNIVERSAL_ENGINE_CO2;
		return -1;
	}

}
