package com.ugs.braci.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.ugs.braci.R;
import com.ugs.info.UIListItemInfo;

public class UISetupListAdapter extends ArrayAdapter<UIListItemInfo> {
	private final String TAG = "UISetupListAdapter";
	Context m_context;
	private int ITEM_LAYOUT = -1;

	public interface Callback {
		public void onItemClick(int p_nPosition);
	}

	private Callback m_callback = null;

	public void setCallback(Callback p_callback) {
		m_callback = p_callback;
	}

	public UISetupListAdapter(Context context, int p_res,
			ArrayList<UIListItemInfo> p_items) {
		super(context, p_res, p_items);

		m_context = context;
		ITEM_LAYOUT = p_res;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View item = convertView;
		final ViewHolder holder;
		final int _position = position;

		// set view
		if (item == null) {
			LayoutInflater vi = (LayoutInflater) getContext().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			item = vi.inflate(ITEM_LAYOUT, null);
			holder = new ViewHolder(item);
			item.setTag(holder);
		} else {
			holder = (ViewHolder) item.getTag();
		}
		item.setId(position);

		// -----------------
		try {
			UIListItemInfo value = getItem(position);
			if (value != null) {

				if (value.m_nResImgID > 0) {
					holder.m_ivImage.setVisibility(View.VISIBLE);
					holder.m_ivImage.setImageResource(value.m_nResImgID);
				} else {
					holder.m_ivImage.setVisibility(View.GONE);
				}

				if (value.m_strTitle != null) {
					holder.m_tvTitle.setText(value.m_strTitle);
				} else {
					holder.m_tvTitle.setText("");
				}

				if (value.m_strContent != null && !value.m_strContent.isEmpty()) {
					holder.m_tvDesc.setText(value.m_strContent);
					holder.m_tvDesc.setVisibility(View.VISIBLE);
				} else {
					holder.m_tvDesc.setVisibility(View.GONE);
				}

				if (value.m_bCheckable) {
					holder.m_tvPreInstalled.setVisibility(View.INVISIBLE);
					holder.m_chBox.setVisibility(View.VISIBLE);
					holder.m_chBox.setChecked(value.m_bChecked);
				} else {
					holder.m_chBox.setVisibility(View.GONE);
					if (_position < getCount() - 1)  {
						holder.m_tvPreInstalled.setVisibility(View.VISIBLE);
					}
				}
			}

			holder.m_ivSelect.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (m_callback != null) {
						m_callback.onItemClick(_position);
					}
				}
			});
		} catch (IndexOutOfBoundsException e) {
			Log.e(TAG, e.getMessage());
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		}

		return item;
	}

	// =====================================================================================================
	/**
	 * UI elements of List item.
	 */
	private class ViewHolder {

		public ImageView m_ivImage;

		public TextView m_tvTitle;
		public TextView m_tvDesc;

		public CheckBox m_chBox;
		public TextView m_tvPreInstalled;

		public ImageView m_ivSelect;

		public ViewHolder(View V) {
			m_ivImage = (ImageView) V.findViewById(R.id.iv_img);

			m_tvTitle = (TextView) V.findViewById(R.id.tv_title);
			m_tvDesc = (TextView) V.findViewById(R.id.tv_desc);

			m_chBox = (CheckBox) V.findViewById(R.id.checkbox);
			m_tvPreInstalled = (TextView) V.findViewById(R.id.tv_pre_installed);

			m_ivSelect = (ImageView) V.findViewById(R.id.iv_select);
		}
	}
}