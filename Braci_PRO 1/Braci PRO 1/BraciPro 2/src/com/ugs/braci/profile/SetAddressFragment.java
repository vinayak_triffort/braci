package com.ugs.braci.profile;

import java.io.IOException;
import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.uc.prjcmn.SharedPreferencesMgr;
import com.ugs.braci.GlobalValues;
import com.ugs.braci.R;
import com.ugs.lib.GPSTracker;

public class SetAddressFragment extends Fragment implements OnClickListener,
		OnMapClickListener {

	private ProfileModeActivity mActivity;

	private Fragment m_oldFragment;
	private String m_strOldTitle;

	private int m_nAddressType = 0;

	private View m_frmAddressBar;
	private EditText m_etPostCode;

	private GoogleMap mGoogleMap;

	private Marker m_markerHome = null;
	private Marker m_markerOffice = null;

	SharedPreferencesMgr m_phoneDb;

	public void setAddressType(int p_nAddressType) {
		m_nAddressType = p_nAddressType;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		final View v = inflater.inflate(R.layout.fragment_set_address,
				container, false);

		mActivity = (ProfileModeActivity) getActivity();

		updateLCD(v);

		onShow();

		// - update position
		if (!PRJFUNC.DEFAULT_SCREEN) {

		}

		return v;
	}

	@Override
	public void onDestroyView() {

		onHide();

		super.onDestroyView();
	}

	public void onShow() {
		m_oldFragment = mActivity.m_curFragment;
		m_strOldTitle = mActivity.m_tvTitle.getText().toString();

		mActivity.m_curFragment = SetAddressFragment.this;

		if (m_nAddressType == PRJCONST.ADDRESS_TYPE_HOME) {
			mActivity.m_tvTitle.setText(getActivity().getString(
					R.string.address_home_title));
		} else if (m_nAddressType == PRJCONST.ADDRESS_TYPE_OFFICE) {
			mActivity.m_tvTitle.setText(getActivity().getString(
					R.string.address_office_title));
		}

		if (m_oldFragment == null) {
			PRJFUNC.hideFragment(mActivity, mActivity.m_profileModeFragment);
		} else {
			PRJFUNC.hideFragment(mActivity, m_oldFragment);
		}

		initMap();
	}

	public void onHide() {
		if (mActivity.m_curFragment == SetAddressFragment.this) {
			mActivity.m_curFragment = m_oldFragment;
			mActivity.m_tvTitle.setText(m_strOldTitle);

			if (mActivity.m_curFragment == null) {
				PRJFUNC.showFragment(mActivity, mActivity.m_profileModeFragment);
			} else {
				PRJFUNC.showFragment(mActivity, m_oldFragment);
			}
		}
	}

	private void initMap() {
		m_phoneDb = new SharedPreferencesMgr(mActivity);
		m_phoneDb.loadHomeLoacation();
		m_phoneDb.loadOfficeLoacation();

		if (mGoogleMap == null) {
			GooglePlayServicesUtil.isGooglePlayServicesAvailable(mActivity);
			mGoogleMap = ((SupportMapFragment) mActivity
					.getSupportFragmentManager().findFragmentById(R.id.map))
					.getMap();

			// 터치이벤트 설정
			mGoogleMap.setOnMapClickListener(this);

			// 마커 클릭 리스너
			mGoogleMap.setOnMarkerClickListener(new OnMarkerClickListener() {

				public boolean onMarkerClick(Marker marker) {
					return false;
				}
			});
		}

		LatLng homePosition = null;
		LatLng officePostiton = null;
		if (GlobalValues.location_home_lat != PRJCONST.INVALID_LOCATION_VALUE) {
			homePosition = new LatLng(GlobalValues.location_home_lat,
					GlobalValues.location_home_lng);
			if (m_markerHome == null) {
				m_markerHome = mGoogleMap.addMarker(new MarkerOptions()
						.position(homePosition).title("Home"));
			} else {
				m_markerHome.setPosition(homePosition);
			}
			m_markerHome.showInfoWindow();

		}

		if (GlobalValues.location_office_lat != PRJCONST.INVALID_LOCATION_VALUE) {
			officePostiton = new LatLng(GlobalValues.location_office_lat,
					GlobalValues.location_office_lng);

			if (m_markerOffice == null) {
				m_markerOffice = mGoogleMap.addMarker(new MarkerOptions()
						.position(officePostiton).title("Office"));
			} else {
				m_markerOffice.setPosition(officePostiton);
			}
			m_markerOffice.showInfoWindow();
		}

		LatLng position = null;
		if (m_nAddressType == PRJCONST.ADDRESS_TYPE_HOME) {
			position = homePosition;
		} else if (m_nAddressType == PRJCONST.ADDRESS_TYPE_OFFICE) {
			position = officePostiton;
		}

		if (PRJFUNC.gpsTracker == null)
			PRJFUNC.gpsTracker = new GPSTracker(mActivity);

		if (position == null) {

			if (PRJFUNC.gpsTracker.canGetLocation()) {
				position = new LatLng(PRJFUNC.gpsTracker.getLatitude(),
						PRJFUNC.gpsTracker.getLongitude());
			} else {
				// can't get location
				// GPS or Network is not enabled
				// Ask user to enable GPS/network in settings
				PRJFUNC.gpsTracker.showSettingsAlert();
			}
		}
		// 맵 위치이동.
		mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 15));

	}

	@Override
	public void onMapClick(LatLng point) {
		setPreAddress(point);
	}

	private void setPreAddress(LatLng point) {
		if (m_nAddressType == PRJCONST.ADDRESS_TYPE_HOME) {
			if (m_markerHome == null) {
				m_markerHome = mGoogleMap.addMarker(new MarkerOptions()
						.position(point).title("Home"));
				m_markerHome.showInfoWindow();
			} else {
				m_markerHome.setPosition(point);
				m_markerHome.showInfoWindow();
			}
		} else if (m_nAddressType == PRJCONST.ADDRESS_TYPE_OFFICE) {
			if (m_markerOffice == null) {
				m_markerOffice = mGoogleMap.addMarker(new MarkerOptions()
						.position(point).title("Office"));
				m_markerOffice.showInfoWindow();
			} else {
				m_markerOffice.setPosition(point);
				m_markerOffice.showInfoWindow();
			}
		}
	}

	// //////////////////////////////////////////////////
	private void updateLCD(View v) {

		if (PRJFUNC.mGrp == null) {
			PRJFUNC.resetGraphValue(mActivity);
		}

		m_frmAddressBar = v.findViewById(R.id.frm_address_bar);
		m_etPostCode = (EditText) v.findViewById(R.id.et_post_code);

		ImageView _iv = (ImageView) v.findViewById(R.id.iv_convert_postal);
		_iv.setOnClickListener(this);

		_iv = (ImageView) v.findViewById(R.id.iv_hide_addressbar);
		_iv.setOnClickListener(this);

		_iv = (ImageView) v.findViewById(R.id.iv_curlocation);
		_iv.setOnClickListener(this);

		_iv = (ImageView) v.findViewById(R.id.iv_ok);
		_iv.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iv_convert_postal:
			onConvertPostalAddress();
			break;
		case R.id.iv_hide_addressbar:
			onHideAddressBar();
			break;
		case R.id.iv_curlocation:
			onCurLocation();
			break;
		case R.id.iv_ok:
			onSetAddress();
			break;

		default:
			break;
		}

	}

	private void onConvertPostalAddress() {
		String strPostalCode = m_etPostCode.getText().toString();
		if (strPostalCode.isEmpty()) {
			Toast.makeText(mActivity,
					mActivity.getString(R.string.address_input_postcode),
					Toast.LENGTH_SHORT).show();
			return;
		}

		final Geocoder geocoder = new Geocoder(mActivity);
		try {
			List<Address> addresses = geocoder.getFromLocationName(
					strPostalCode, 1);
			if (addresses != null && !addresses.isEmpty()) {
				Address address = addresses.get(0);
				LatLng position = new LatLng(address.getLatitude(),
						address.getLongitude());
				setPreAddress(position);

				mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
						position, 15));
			} else {
				Toast.makeText(mActivity, "Unable to get Location",
						Toast.LENGTH_LONG).show();
			}
		} catch (IOException e) {
			Toast.makeText(mActivity,
					"Exception occured while getting location",
					Toast.LENGTH_LONG).show();
		}

		InputMethodManager imm = (InputMethodManager) mActivity
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(m_etPostCode.getWindowToken(), 0);
	}

	private void onHideAddressBar() {
		m_frmAddressBar.setVisibility(View.GONE);
	}

	private void onCurLocation() {
		PRJFUNC.gpsTracker.getLocation();

		if (PRJFUNC.gpsTracker.canGetLocation()) {
			double lat = PRJFUNC.gpsTracker.getLatitude();
			double lng = PRJFUNC.gpsTracker.getLongitude();
			LatLng position = new LatLng(lat, lng);

			mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position,
					15));

			setPreAddress(position);
		} else {
			// can't get location
			// GPS or Network is not enabled
			// Ask user to enable GPS/network in settings
			PRJFUNC.gpsTracker.showSettingsAlert();
		}
	}

	private void setHomeAddress() {
		LatLng position = m_markerHome.getPosition();
		GlobalValues.location_home_lat = position.latitude;
		GlobalValues.location_home_lng = position.longitude;
		m_phoneDb.saveHomeLocation();

		mActivity.goBackForSetAddress(m_nAddressType);
	}

	private void setOfficeAddress() {
		LatLng position = m_markerOffice.getPosition();
		GlobalValues.location_office_lat = position.latitude;
		GlobalValues.location_office_lng = position.longitude;
		m_phoneDb.saveOfficeLocation();

		mActivity.goBackForSetAddress(m_nAddressType);
	}

	private void onSetAddress() {
		if (m_nAddressType == PRJCONST.ADDRESS_TYPE_HOME) {
			if (m_markerHome != null) {

				if (GlobalValues.location_home_lat == PRJCONST.INVALID_LOCATION_VALUE) {
					setHomeAddress();
				} else {
					AlertDialog.Builder alertDialog = new AlertDialog.Builder(
							mActivity);
					alertDialog.setTitle(R.string.address_btn_set);
					alertDialog.setMessage(R.string.address_change_confirm);
					alertDialog.setPositiveButton(R.string.word_yes,
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									setHomeAddress();
								}
							});
					alertDialog.setNegativeButton(R.string.word_no, null);
					alertDialog.show();
				}

			} else {
				AlertDialog.Builder alertDialog = new AlertDialog.Builder(
						mActivity);
				alertDialog.setTitle(R.string.address_btn_set);
				alertDialog.setMessage(R.string.address_not_indicated);
				alertDialog.setPositiveButton(R.string.word_ok, null);
				alertDialog.show();
				return;
			}
		} else if (m_nAddressType == PRJCONST.ADDRESS_TYPE_OFFICE) {
			if (m_markerOffice != null) {
				if (GlobalValues.location_office_lat == PRJCONST.INVALID_LOCATION_VALUE) {
					setOfficeAddress();
				} else {
					AlertDialog.Builder alertDialog = new AlertDialog.Builder(
							mActivity);
					alertDialog.setTitle(R.string.address_btn_set);
					alertDialog.setMessage(R.string.address_change_confirm);
					alertDialog.setPositiveButton(R.string.word_yes,
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									setOfficeAddress();
								}
							});
					alertDialog.setNegativeButton(R.string.word_no, null);
					alertDialog.show();
				}
			} else {
				AlertDialog.Builder alertDialog = new AlertDialog.Builder(
						mActivity);
				alertDialog.setTitle(R.string.address_btn_set);
				alertDialog.setMessage(R.string.address_not_indicated);
				alertDialog.setPositiveButton(R.string.word_ok, null);
				alertDialog.show();
				return;
			}
		}

	}

}
