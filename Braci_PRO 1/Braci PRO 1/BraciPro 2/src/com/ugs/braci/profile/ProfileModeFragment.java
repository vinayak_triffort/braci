package com.ugs.braci.profile;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;

import com.uc.prjcmn.PRJFUNC;
import com.uc.prjcmn.SharedPreferencesMgr;
import com.ugs.braci.GlobalValues;
import com.ugs.braci.R;
import com.ugs.lib.GPSTracker;
import com.ugs.service.BraciProService;

public class ProfileModeFragment extends Fragment implements OnClickListener,
		OnCheckedChangeListener {

	private ProfileModeActivity mActivity;

	private CheckBox m_chProfileMode;

	SharedPreferencesMgr m_phoneDb = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		final View v = inflater.inflate(R.layout.fragment_profile_mode,
				container, false);

		mActivity = (ProfileModeActivity) getActivity();

		m_phoneDb = new SharedPreferencesMgr(mActivity);

		updateLCD(v);

		// - update position
		if (!PRJFUNC.DEFAULT_SCREEN) {

		}

		return v;
	}

	// //////////////////////////////////////////////////
	private void updateLCD(View v) {

		if (PRJFUNC.mGrp == null) {
			PRJFUNC.resetGraphValue(mActivity);
		}

		m_chProfileMode = (CheckBox) v.findViewById(R.id.ch_profile_mode);
		m_chProfileMode.setChecked(GlobalValues.m_bProfileAutoSel);
		m_chProfileMode.setOnCheckedChangeListener(this);

		ImageView _iv = null;
		_iv = (ImageView) v.findViewById(R.id.iv_profile_mode);
		_iv.setOnClickListener(this);

		_iv = (ImageView) v.findViewById(R.id.iv_indoor);
		_iv.setOnClickListener(this);

		_iv = (ImageView) v.findViewById(R.id.iv_outdoor);
		_iv.setOnClickListener(this);

		_iv = (ImageView) v.findViewById(R.id.iv_edit_address);
		_iv.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iv_profile_mode:
			onSwitchProfileMode();
			break;
		case R.id.iv_indoor:
			mActivity.onIndoorModePressed();
			break;

		case R.id.iv_outdoor:
			mActivity.onOutdoorModePressed();
			break;

		case R.id.iv_edit_address:
			mActivity.onEditAddressPressed();
			break;

		default:
			break;
		}

	}

	private void onSwitchProfileMode() {
		m_chProfileMode.setChecked(!m_chProfileMode.isChecked());
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		GlobalValues.m_bProfileAutoSel = isChecked;
		if (GlobalValues.m_bProfileAutoSel) {
			BraciProService.times_for_location = 0;
			PRJFUNC.gpsTracker = new GPSTracker(mActivity);
			if (!PRJFUNC.gpsTracker.canGetLocation()) {
				PRJFUNC.gpsTracker.showSettingsAlert();
			}
		} else {
			if (PRJFUNC.gpsTracker != null) {
				PRJFUNC.gpsTracker.stopUsingGPS();
				PRJFUNC.gpsTracker = null;
			}
		}

		m_phoneDb.saveProfileAutoSel();
	}
}
