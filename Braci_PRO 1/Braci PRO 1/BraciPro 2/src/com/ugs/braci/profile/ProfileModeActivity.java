package com.ugs.braci.profile;

import java.io.File;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.uc.prjcmn.ActivityTask;
import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.ugs.braci.GlobalValues;
import com.ugs.braci.R;

public class ProfileModeActivity extends FragmentActivity implements
		OnClickListener {

	public static final String AUTO_OPENED = "auto_opened";
	public static final String SETUP_PROFILE_MODE = "recorded_profile_mode";
	private final int PAGE_ALL_DONE = 1;

	private boolean m_bAutoOpened = false;
	private int m_nProfileMode = -1;

	private Context mContext;

	public ProfileModeFragment m_profileModeFragment;
	public Fragment m_curFragment;

	public SetAddressFragment m_addressFragment;

	public TextView m_tvTitle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setup);
		ActivityTask.INSTANCE.add(this);

		mContext = this;

		m_bAutoOpened = getIntent().getBooleanExtra(AUTO_OPENED, false);
		m_nProfileMode = getIntent().getIntExtra(SETUP_PROFILE_MODE, -1);

		initValues();

		updateLCD();

		m_tvTitle.setText(getString(R.string.menu_profile_mode));

		m_profileModeFragment = new ProfileModeFragment();
		PRJFUNC.addFragment(ProfileModeActivity.this, m_profileModeFragment);

		if (m_bAutoOpened) {
			if (m_nProfileMode < 0) {
				onEditAddressPressed();
			} else if (m_nProfileMode == PRJCONST.PROFILE_MODE_HOME) {
				onSetAddressFragment(PRJCONST.ADDRESS_TYPE_HOME);
			} else if (m_nProfileMode == PRJCONST.PROFILE_MODE_OFFICE) {
				onSetAddressFragment(PRJCONST.ADDRESS_TYPE_OFFICE);
			}
		}

		m_curFragment = null;
	}

	private void initValues() {

	}

	@Override
	protected void onDestroy() {
		PRJFUNC.closeProgress(mContext);

		ActivityTask.INSTANCE.remove(this);

		releaseValues();

		super.onDestroy();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {

		if (requestCode == PAGE_ALL_DONE) {
			if (resultCode == RESULT_OK) {

				if (m_bAutoOpened) {
					new Handler().postDelayed(new Runnable() {
						@Override
						public void run() {
							finish();
						}
					}, 10);
				}
			}
		}

		super.onActivityResult(requestCode, resultCode, intent);
	}

	private void releaseValues() {
		m_profileModeFragment = null;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			goBack();
			return false;
		}

		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	private void updateLCD() {
		m_tvTitle = (TextView) findViewById(R.id.tv_title);
		// PRJFUNC.setTextViewFont(mContext, m_tvTitle,
		// PRJCONST.FONT_AuctionGothicBold);

		ImageView _iv = (ImageView) findViewById(R.id.iv_back);
		_iv.setOnClickListener(this);

		_iv = (ImageView) findViewById(R.id.iv_help_menu);
		_iv.setOnClickListener(this);
		_iv.setVisibility(View.INVISIBLE);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iv_back:
			goBack();
			break;
		case R.id.iv_help_menu:
			onHelpMenu();
			break;
		default:
			break;
		}
	}

	private void onHelpMenu() {
		// TODO Auto-generated method stub

	}

	public void goBackForSetAddress(int p_nSetProfileMode) {
		goBack();
		if (m_bAutoOpened) {
			boolean bAllDone = false;
			if (m_nProfileMode < 0) {
				if (p_nSetProfileMode == PRJCONST.PROFILE_MODE_OFFICE) {
					File dirOffice = new File(
							PRJFUNC.getRecordRootDir(PRJCONST.PROFILE_MODE_OFFICE));
					File dirHome = new File(
							PRJFUNC.getRecordRootDir(PRJCONST.PROFILE_MODE_HOME));

					if (!dirOffice.exists() && dirHome.exists()) {
						dirHome.renameTo(dirOffice);
					}
				}
				bAllDone = true;
			} else if (m_nProfileMode == PRJCONST.PROFILE_MODE_HOME) {
				if (GlobalValues.location_home_lat != PRJCONST.INVALID_LOCATION_VALUE) {
					bAllDone = true;
				}
			} else if (m_nProfileMode == PRJCONST.PROFILE_MODE_OFFICE) {
				if (GlobalValues.location_office_lat != PRJCONST.INVALID_LOCATION_VALUE) {
					bAllDone = true;
				}
			}

			if (bAllDone) {
				Intent intent = new Intent(ProfileModeActivity.this,
						AllDoneActivity.class);
				startActivityForResult(intent, PAGE_ALL_DONE);
			}
		}
	}

	public void goBack() {
		if (m_curFragment != null) {
			if (m_curFragment == m_addressFragment) {
				PRJFUNC.hideFragment(ProfileModeActivity.this, m_curFragment);
				m_addressFragment.onHide();
			} else {
				PRJFUNC.removeFragment(ProfileModeActivity.this, m_curFragment);
			}
		} else {
			finish();
			overridePendingTransition(R.anim.hold, R.anim.right_out);
		}
	}

	public void onIndoorModePressed() {
		IndoorModeFragment fragment = new IndoorModeFragment();
		PRJFUNC.addFragment(ProfileModeActivity.this, fragment);
	}

	public void onOutdoorModePressed() {
		OutdoorModeFragment fragment = new OutdoorModeFragment();
		PRJFUNC.addFragment(ProfileModeActivity.this, fragment);
	}

	public void onEditAddressPressed() {
		EditAddressFragment fragment = new EditAddressFragment();
		PRJFUNC.addFragment(ProfileModeActivity.this, fragment);
	}

	public void onSetAddressFragment(int p_nAddressType) {
		if (m_addressFragment == null) {
			m_addressFragment = new SetAddressFragment();
			m_addressFragment.setAddressType(p_nAddressType);
			PRJFUNC.addFragment(ProfileModeActivity.this, m_addressFragment);
		} else {
			m_addressFragment.setAddressType(p_nAddressType);
			m_addressFragment.onShow();
			PRJFUNC.showFragment(ProfileModeActivity.this, m_addressFragment);
		}
	}
}
