package com.ugs.braci.login;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.uc.prjcmn.ActivityTask;
import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.uc.prjcmn.SharedPreferencesMgr;
import com.ugs.braci.GlobalValues;
import com.ugs.braci.HomeActivity;
import com.ugs.braci.MainApplication;
import com.ugs.braci.R;
import com.ugs.braci.photoslider.PhotoSliderActivity;

public class LoginActivity extends Activity implements OnClickListener {

	public static final boolean REQUIRED_EMAIL_VERIFIED = true;

	private final String TAG = "_LoginActivity";
	private final int SIGNUP_ACTIVITY = 31393;
	private final int RESET_PWD_ACTIVITY = 31394;

	private EditText m_etUsername;
	private EditText m_etPwd;

	private CheckBox m_chkRemember;

	private Context mContext;

	private SharedPreferencesMgr pPhoneDb;

	// ////////////////////////////////////////////////////////////
	// //////////////////////////////////////////////////////////////
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		ActivityTask.INSTANCE.add(this);

		mContext = (Context) this;

		updateLCD();

		// - update position
		if (!PRJFUNC.DEFAULT_SCREEN) {
			scaleView();
		}

		pPhoneDb = ((MainApplication) getApplication())
				.getSharedPreferencesMgrPoint();
	}

	@Override
	protected void onDestroy() {
		releaseValues();

		ActivityTask.INSTANCE.remove(this);

		super.onDestroy();
	}

	private void releaseValues() {

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			goMainActivity();
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}

	// //////////////////////////////////////////////////
	private void updateLCD() {

		if (PRJFUNC.mGrp == null) {
			PRJFUNC.resetGraphValue(mContext);
		}

		m_etUsername = (EditText) findViewById(R.id.et_username);
		m_etPwd = (EditText) findViewById(R.id.et_password);

		int hintColor = Color.rgb(0xB2, 0xC1, 0xC9);
		m_etUsername.setHintTextColor(hintColor);
		m_etPwd.setHintTextColor(hintColor);

		TextView _tv = (TextView) findViewById(R.id.tv_forgot_pw);
		PRJFUNC.setTextViewFont(mContext, _tv, PRJCONST.FONT_OpenSans);
		_tv.setOnClickListener(this);

		ImageView _iv = (ImageView) findViewById(R.id.iv_login);
		_iv.setOnClickListener(this);

		_iv = (ImageView) findViewById(R.id.iv_signup);
		_iv.setOnClickListener(this);

		m_chkRemember = (CheckBox) findViewById(R.id.ch_remember);
		final float scale = this.getResources().getDisplayMetrics().density;
		m_chkRemember.setPadding(m_chkRemember.getPaddingLeft()
				+ (int) (30.0f * scale + 0.5f), m_chkRemember.getPaddingTop(),
				m_chkRemember.getPaddingRight(),
				m_chkRemember.getPaddingBottom());

		if (GlobalValues.m_stUserName != null) {
			m_etUsername.setText(GlobalValues.m_stUserName);
		}
		if (GlobalValues.m_stPassword != null) {
			m_etPwd.setText(GlobalValues.m_stPassword);
		}
	}

	private void scaleView() {

		if (PRJFUNC.mGrp == null) {
			return;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == SIGNUP_ACTIVITY) {
			if (resultCode == RESULT_OK) {
				if (GlobalValues.m_stUserName != null) {
					m_etUsername.setText(GlobalValues.m_stUserName);
				}
				if (GlobalValues.m_stPassword != null) {
					m_etPwd.setText(GlobalValues.m_stPassword);
				}
			}
		} else if (requestCode == RESET_PWD_ACTIVITY) {

		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	// /////////////////////////////////////
	private void goMainActivity() {
		finish();
		overridePendingTransition(R.anim.from_bottom, R.anim.hold);
	}

	@Override
	public void onClick(View v) {
		GlobalValues.m_stUserName = m_etUsername.getText().toString();
		GlobalValues.m_stPassword = m_etPwd.getText().toString();

		switch (v.getId()) {
		case R.id.tv_forgot_pw:
			onForgetPwd();
			break;

		case R.id.iv_login:
			if (confrim()) {
				onLogin();
			}
			break;

		case R.id.iv_signup:
			goSignupActivity();
			break;

		default:
			break;
		}
	}

	private void goSignupActivity() {
		Intent intent = new Intent(LoginActivity.this, SignupActivity.class);
		startActivityForResult(intent, SIGNUP_ACTIVITY);
	}

	private void onForgetPwd() {
		Intent intent = new Intent(LoginActivity.this, ResetPwdActivity.class);
		startActivityForResult(intent, RESET_PWD_ACTIVITY);
	}

	private void onLogin() {
		ParseUser.logInInBackground(GlobalValues.m_stUserName,
				GlobalValues.m_stPassword, new LogInCallback() {
					public void done(ParseUser user, ParseException e) {
						if (user != null) {
							boolean bEmailVerified = user
									.getBoolean("emailVerified");
							if (bEmailVerified || !REQUIRED_EMAIL_VERIFIED) {

								pPhoneDb.saveUserInfo(
										GlobalValues.m_stUserName,
										GlobalValues.m_stPassword);

								goToHomeActivity();
							} else {
								AlertDialog.Builder dlgAlert = new AlertDialog.Builder(
										mContext);

								dlgAlert.setMessage(mContext
										.getString(R.string.verify_email));
								dlgAlert.setTitle("Braci");
								dlgAlert.setPositiveButton("OK", null);
								dlgAlert.setCancelable(true);
								dlgAlert.create().show();
							}

						} else {
							// Login failed. Look at the ParseException to see
							// what happened.
							AlertDialog.Builder dlgAlert = new AlertDialog.Builder(
									mContext);

							dlgAlert.setMessage(e.getMessage());
							dlgAlert.setTitle("Braci");
							dlgAlert.setPositiveButton("OK", null);
							dlgAlert.setCancelable(true);
							dlgAlert.create().show();
						}
					}
				});
	}

	private boolean confrim() {

		if (GlobalValues.m_stUserName.equals("")) {
			AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);

			dlgAlert.setMessage("Please input username");
			dlgAlert.setTitle("Braci");
			dlgAlert.setPositiveButton("OK", null);
			dlgAlert.setCancelable(true);
			dlgAlert.create().show();
			return false;
		} else if (GlobalValues.m_stUserName.equals("")) {
			AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);

			dlgAlert.setMessage("Please input Password");
			dlgAlert.setTitle("Braci");
			dlgAlert.setPositiveButton("OK", null);
			dlgAlert.setCancelable(true);
			dlgAlert.create().show();

			return false;
		}
		return true;
	}

	protected void goToPhotoSlidingActivity() {
		Intent it = new Intent(LoginActivity.this, PhotoSliderActivity.class);
		startActivity(it);
		finish();
	}

	protected void goToHomeActivity() {
		Intent it = new Intent(LoginActivity.this, HomeActivity.class);
		startActivity(it);
		finish();
	}

}
