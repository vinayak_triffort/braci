package com.ugs.braci;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.util.Calendar;
import java.util.GregorianCalendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.TextView;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.uc.prjcmn.ActivityTask;
import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.uc.prjcmn.RS;
import com.uc.prjcmn.SharedPreferencesMgr;
import com.ugs.braci.engine.SoundEngine;
import com.ugs.braci.help.TermsActivity;
import com.ugs.braci.login.LoginActivity;
import com.ugs.braci.photoslider.PhotoSliderActivity;
import com.ugs.braci.R;
import com.ugs.lib.GPSTracker;
import com.ugs.lib.LayoutLib;

public class _SplashActivity extends Activity implements OnClickListener {

	private final String TAG = "_SplashActivity";

	private Context mContext;
	SharedPreferencesMgr m_pPhoneDb;

	public boolean m_bResume;

	// ////////////////////////////////////////////////////////////
	// //////////////////////////////////////////////////////////////
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		ActivityTask.INSTANCE.add(this);

		mContext = (Context) this;

		getScreenInfo();

		m_pPhoneDb = ((MainApplication) getApplication())
				.getSharedPreferencesMgrPoint();
		m_pPhoneDb.setScreenInfo();

		updateLCD();

		m_bResume = false;

		// - update position
		if (!PRJFUNC.DEFAULT_SCREEN) {
			scaleView();
		}

		if (GlobalValues.m_stUserName == null || GlobalValues.m_stUserName.isEmpty()) {
			m_pPhoneDb.loadUserInfo();
		} else {
			m_bResume = true;
		}

		if (getTimeCheckFile().exists()) {
			if (!isValidTime() && !PRJCONST.IsIgnoreExpiration) {
				AlertDialog.Builder dlgAlert = new AlertDialog.Builder(mContext);
				dlgAlert.setMessage(mContext.getString(R.string.time_expired));
				dlgAlert.setTitle(getString(R.string.app_name_short));
				dlgAlert.setPositiveButton(getString(R.string.word_ok), null);
				dlgAlert.setCancelable(true);
				dlgAlert.create().show();
				return;
			}
		} else {
			setStartTime();
		}

		if (SoundEngine.DELTA_MATCH_RATES == null) {
			m_bResume = false;
		}

		if (!m_bResume) {
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					int nDbVersion = m_pPhoneDb.getDbVersion();
					if (nDbVersion != PRJCONST.DB_VERSION) {
						PRJFUNC.deleteFile(new File(PRJFUNC.getDeafRootDir()),
								false);
						PRJFUNC.extractDeafData(mContext);

						PRJFUNC.releaseDetectDataFromDb(mContext);
						m_pPhoneDb.setCurDbVersion();
					}

					if (SoundEngine.DELTA_MATCH_RATES == null) {
						SoundEngine.DELTA_MATCH_RATES = m_pPhoneDb
								.loadMatchingRatesDelta(PRJFUNC
										.getCurProfileMode());
					}
					if (GlobalValues.recordedDetectData == null) {
						PRJFUNC.initRecordedData(_SplashActivity.this,
								GlobalValues.m_nProfileIndoorMode);
					}

					SoundEngine.THIEF_ALARM_IDX = m_pPhoneDb.loadTheifAlarmIndex(GlobalValues.m_nProfileIndoorMode);
					
					PRJFUNC.initDetectData(_SplashActivity.this);

					if (GlobalValues._firstRun) {
						goToTermsActivity();
					} else {
						if (GlobalValues.m_stUserName.equals("")
								&& PRJCONST.IsInternetVersion) {
							goToPhotoSlidingActivity();
						} else {
							onAutoLogin();
						}
					}
				}

			}, 1000);
		} else {
			goToHomeActivity();
		}
	}

	@Override
	protected void onDestroy() {
		releaseValues();

		ActivityTask.INSTANCE.remove(this);

		super.onDestroy();
	}

	private void releaseValues() {

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			goBack();
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}

	// //////////////////////////////////////////////////
	private void onAutoLogin() {
		ParseUser.logInInBackground(GlobalValues.m_stUserName,
				GlobalValues.m_stPassword, new LogInCallback() {
					public void done(ParseUser user, ParseException e) {
						if (user != null) {
							boolean bEmailVerified = user
									.getBoolean("emailVerified");
							if (bEmailVerified
									|| !LoginActivity.REQUIRED_EMAIL_VERIFIED) {
								goToHomeActivity();
							} else {
								goToHomeActivity();

								// AlertDialog.Builder dlgAlert = new
								// AlertDialog.Builder(
								// mContext);
								//
								// dlgAlert.setMessage(mContext.getString(R.string.verify_email));
								// dlgAlert.setTitle("Braci");
								// dlgAlert.setPositiveButton("OK", null);
								// dlgAlert.setCancelable(true);
								// dlgAlert.create().show();
							}
						} else {
							// Login failed. Look at the ParseException to see
							// what happened.

							goToHomeActivity();

							// AlertDialog.Builder dlgAlert = new
							// AlertDialog.Builder(
							// mContext);
							//
							// dlgAlert.setMessage(e.getMessage());
							// dlgAlert.setTitle("Braci");
							// dlgAlert.setPositiveButton("OK", null);
							// dlgAlert.setCancelable(true);
							// dlgAlert.create().show();
						}
					}
				});

	}

	private void updateLCD() {

		if (PRJFUNC.mGrp == null) {
			PRJFUNC.resetGraphValue(mContext);
		}

		TextView _tv = (TextView) findViewById(R.id.tv_test);
		PRJFUNC.setTextViewFont(mContext, _tv, PRJCONST.FONT_OpenSansBold);
		_tv.setOnClickListener(this);

		_tv = (TextView) findViewById(R.id.tv_normal);
		PRJFUNC.setTextViewFont(mContext, _tv, PRJCONST.FONT_OpenSansBold);
		_tv.setOnClickListener(this);

		View _v = (View) findViewById(R.id.frm_buttons);
		if (!PRJCONST.IsTestVersion) {
			_v.setVisibility(View.INVISIBLE);
		}

	}

	private void scaleView() {

		if (PRJFUNC.mGrp == null) {
			return;
		}
	}

	// /////////////////////////////////////
	private void goBack() {
		finish();
		// overridePendingTransition(R.anim.from_bottom, R.anim.hold);
	}

	private void getScreenInfo() {

		int n1;

		// 상태바
		Rect rectgle = new Rect();
		Window window = getWindow();
		window.getDecorView().getWindowVisibleDisplayFrame(rectgle);
		int StatusBarHeight = rectgle.top;
		PRJFUNC.H_STATUSBAR = StatusBarHeight;

		DisplayMetrics displayMetrics = getResources().getDisplayMetrics();

		PRJFUNC.DPI = displayMetrics.densityDpi;
		PRJFUNC.W_LCD = displayMetrics.widthPixels;
		PRJFUNC.H_LCD = displayMetrics.heightPixels;
		if (PRJFUNC.W_LCD > PRJFUNC.H_LCD) {
			// 만일 너비가 높이보다 작다면 비정상;
			// 바꾸어준다.

			n1 = PRJFUNC.W_LCD;
			PRJFUNC.W_LCD = PRJFUNC.H_LCD;
			PRJFUNC.H_LCD = n1;
		}

		// -- 확대비율
		float f1, f2;
		f1 = PRJFUNC.W_LCD / (PRJCONST.SCREEN_WIDTH * 1.0f);
		f2 = PRJFUNC.H_LCD / (PRJCONST.SCREEN_HEIGHT * 1.0f);
		RS.X_Z = f1;
		RS.Y_Z = f2;

		// -
		PRJFUNC.mGrp = new LayoutLib(PRJCONST.SCREEN_DPI, PRJFUNC.DPI, f1, f2,
				displayMetrics.density);

		// - font size
		if (PRJFUNC.W_LCD == 800 && PRJFUNC.H_LCD == 1232) { // 갤럭시탭 10.1인치
			RS.DEFAULT_FONTSIZE = 25;
		} else if (PRJFUNC.W_LCD == 480) {
			RS.DEFAULT_FONTSIZE = 12;
		}

		// -
		if (PRJFUNC.W_LCD == PRJCONST.SCREEN_WIDTH
				&& PRJFUNC.H_LCD == PRJCONST.SCREEN_HEIGHT
				&& PRJFUNC.DPI == PRJCONST.SCREEN_DPI) {
			PRJFUNC.DEFAULT_SCREEN = true;
		} else {
			PRJFUNC.DEFAULT_SCREEN = false;
		}
	}

	private void goToLoginActivity() {
		Intent intent = new Intent(_SplashActivity.this, LoginActivity.class);
		startActivity(intent);
		finish();
	}

	protected void goToTermsActivity() {
		Intent it = new Intent(_SplashActivity.this, TermsActivity.class);
		startActivity(it);
		finish();
	}

	protected void goToHomeActivity() {
		Intent it = new Intent(_SplashActivity.this, HomeActivity.class);
		startActivity(it);
		finish();
	}

	protected void goToPhotoSlidingActivity() {
		Intent it = new Intent(_SplashActivity.this, PhotoSliderActivity.class);
		startActivity(it);
		finish();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		default:
			break;
		}
	}

	private File getTimeCheckFile() {
		File file = Environment.getExternalStorageDirectory();
		String strPath = file.getPath() + "/bp11789.dat";
		file = new File(strPath);
		return file;
	}

	private boolean isValidTime() {
		File file = getTimeCheckFile();
		if (!file.exists()) {
			return false;
		}

		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line = br.readLine();
			long timeInstalled = Long.parseLong(line);
			Calendar now = GregorianCalendar.getInstance();
			long timeInNow = now.getTimeInMillis();
			long timeDifferent = (timeInNow - timeInstalled) / 1000 / 60;
			if (timeDifferent < PRJCONST.FREE_USE_DAYS * 24 * 60)
				return true;
		} catch (Exception e) {
			Log.i(TAG, e.getMessage());
		}
		return false;
	}

	public void setStartTime() {
		try {
			FileOutputStream fos = null;
			File file = getTimeCheckFile();
			fos = new FileOutputStream(file);
			long nNowInMili = GregorianCalendar.getInstance().getTimeInMillis();
			String str = nNowInMili + "\n";
			byte[] data = str.getBytes();
			fos.write(data);
			fos.close();
		} catch (Exception e) {

		}
	}
}
