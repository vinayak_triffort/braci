package com.ugs.braci.setup;

import java.io.File;
import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.uc.prjcmn.SharedPreferencesMgr;
import com.ugs.braci.GlobalValues;
import com.ugs.braci.R;
import com.ugs.braci.adapter.UIListAdapter;
import com.ugs.braci.adapter.UISetupListAdapter;
import com.ugs.braci.engine.DetectingData;
import com.ugs.info.UIListItemInfo;

public class SetupFragment extends Fragment implements OnClickListener {

	private SetupActivity mActivity;

	private ListView m_listItems;
	private UISetupListAdapter m_adapterItems;

	private TextView m_tvDescription;

	private int m_nCurIdx = -1;

	private final int ITEM_SETUP_DEAF_PRODUCTS = PRJCONST.REC_SOUND_TYPE_CNT;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		final View v = inflater.inflate(R.layout.fragment_setup, container,
				false);

		mActivity = (SetupActivity) getActivity();

		updateLCD(v);

		// - update position
		if (!PRJFUNC.DEFAULT_SCREEN) {

		}

		m_nCurIdx = -1;

		return v;
	}

	private int[] m_imgSoundIds = { R.drawable.ic_setup_doorbell,
			R.drawable.ic_setup_doorbell, R.drawable.ic_setup_smokealarm,
			R.drawable.ic_setup_landline, R.drawable.ic_setup_intercom,
			R.drawable.ic_setup_carbon, R.drawable.ic_setup_alarmclock,
			R.drawable.ic_setup_thief, R.drawable.ic_setup_microwave };

	private int[] m_nSoundTitleIds = { R.string.setup_doorbell_title,
			R.string.setup_backdoorbell_title, R.string.setup_smokealarm_title,
			R.string.setup_landline_title, R.string.setup_intercom_title,
			R.string.setup_carbon_title, R.string.setup_alarmclock_title,
			R.string.setup_thief_title, R.string.setup_microwave_title };

	private int[] m_nSoundContentIds = { R.string.setup_doorbell_desc,
			R.string.setup_backdoorbell_desc, R.string.setup_smokealarm_desc,
			R.string.setup_landline_desc, R.string.setup_intercom_desc,
			R.string.setup_carbon_desc, R.string.setup_alarmclock_desc,
			R.string.setup_thief_desc, R.string.setup_microwave_desc };

	public void refreshList() {
		m_adapterItems.clear();

		UIListItemInfo itemInfo = null;

		if (GlobalValues._bRecordedSoundsDetectable == null) {
			SharedPreferencesMgr phoneDb = new SharedPreferencesMgr(mActivity);
			phoneDb.loadRecordedDetectableInfo();
		}

		for (int type = 0; type < PRJCONST.REC_SOUND_TYPE_CNT; type++) {
			if (SetupActivity.m_bSoundsRecordable[type])
				continue;
			itemInfo = new UIListItemInfo(m_imgSoundIds[type],
					mActivity.getString(m_nSoundTitleIds[type]),
					mActivity.getString(m_nSoundContentIds[type]), false);
			itemInfo.m_extraData = (Integer) type;
			itemInfo.setChecked(false);
			m_adapterItems.add(itemInfo);
		}

		for (int type = 0; type < PRJCONST.REC_SOUND_TYPE_CNT; type++) {
			if (!SetupActivity.m_bSoundsRecordable[type]) 
				continue;
			
			itemInfo = new UIListItemInfo(m_imgSoundIds[type],
					mActivity.getString(m_nSoundTitleIds[type]),
					mActivity.getString(m_nSoundContentIds[type]), true);
			itemInfo.m_extraData = (Integer) type;
			itemInfo.setChecked(false);
			m_adapterItems.add(itemInfo);
		}

		itemInfo = new UIListItemInfo(R.drawable.ic_setup_deaf,
				mActivity.getString(R.string.setup_deaf_title),
				mActivity.getString(R.string.setup_deaf_desc), false);
		itemInfo.setChecked(false);
		m_adapterItems.add(itemInfo);
	}

	public void refreshCheckBoxes() {
/*
		if (mActivity.isFirstRecord(mActivity.m_nProfileModeForRecording)) {
			// Check all if it is first time of recording
			for (int i = 0; i < m_adapterItems.getCount(); i++) {
				UIListItemInfo itemInfo = m_adapterItems.getItem(i);
				itemInfo.setChecked(true);
			}
		} else {
			for (int i = 0; i < m_adapterItems.getCount(); i++) {
				UIListItemInfo itemInfo = m_adapterItems.getItem(i);
				if (itemInfo.m_extraData == null)
					continue;
				int nSoundType = (Integer) itemInfo.m_extraData;
				if (!mActivity.isExistSound(nSoundType)
						&& SetupActivity.m_bSoundsRecordable[nSoundType]) {
					itemInfo.setChecked(true);
				} else {
					itemInfo.setChecked(false);
				}
			}
		}
		m_adapterItems.notifyDataSetChanged();
*/
	}

	// //////////////////////////////////////////////////
	private void updateLCD(View v) {

		if (PRJFUNC.mGrp == null) {
			PRJFUNC.resetGraphValue(mActivity);
		}

		m_tvDescription = (TextView) v.findViewById(R.id.tv_desc);

		m_listItems = (ListView) v.findViewById(R.id.listview);
		m_adapterItems = new UISetupListAdapter(mActivity,
				R.layout.list_item_setup,
				new ArrayList<UIListItemInfo>());
		m_adapterItems.setCallback(m_adapterCallback);
		m_listItems.setAdapter(m_adapterItems);

		refreshList();

		ImageView _iv = (ImageView) v.findViewById(R.id.iv_btn_next);
		_iv.setOnClickListener(this);

		if (mActivity.m_bAutoOpened) {
			_iv = (ImageView) v.findViewById(R.id.iv_btn_notnow);
			_iv.setOnClickListener(this);
		} else {
			View _v = v.findViewById(R.id.frm_btn_notnow);
			_v.setVisibility(View.GONE);
		}

		refreshCheckBoxes();
	}

	UISetupListAdapter.Callback m_adapterCallback = new UISetupListAdapter.Callback() {
		@Override
		public void onItemClick(int p_nPosition) {
			if (p_nPosition < ITEM_SETUP_DEAF_PRODUCTS) {
				final UIListItemInfo itemInfo = m_adapterItems
						.getItem(p_nPosition);
				if (itemInfo.m_bChecked == false
						&& itemInfo.m_extraData != null) {

					if (mActivity.isExistSound((Integer) itemInfo.m_extraData)) {
						AlertDialog.Builder alertDialog = new AlertDialog.Builder(
								mActivity);
						alertDialog.setTitle(R.string.setup_title);
						alertDialog.setMessage(R.string.recorded_delete_sound);
						alertDialog.setPositiveButton(R.string.word_yes,
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										int nSndType = (Integer) itemInfo.m_extraData;
										PRJFUNC.deleteFile(
												new File(
														PRJFUNC.getRecordDir(
																nSndType,
																mActivity.m_nProfileModeForRecording)),
												false);
										if (mActivity.m_nProfileModeForRecording == GlobalValues.m_nProfileIndoorMode) {
											ArrayList<DetectingData> detectingDataList = PRJFUNC
													.getRecordingDataList(nSndType);
											if (detectingDataList != null)
												detectingDataList.clear();
										}
										itemInfo.m_bChecked = !itemInfo.m_bChecked;
										m_adapterItems.notifyDataSetChanged();
									}
								});
						alertDialog.setNegativeButton(R.string.word_no, null);
						alertDialog.show();
					} else {
						itemInfo.m_bChecked = !itemInfo.m_bChecked;
						m_adapterItems.notifyDataSetChanged();
					}
				} else {
					itemInfo.m_bChecked = !itemInfo.m_bChecked;
					m_adapterItems.notifyDataSetChanged();
				}
			}

			if (p_nPosition == ITEM_SETUP_DEAF_PRODUCTS) {
				mActivity.onDeafProductsSelected();
			}
		}
	};

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iv_btn_next:
			onNext();
			break;
		case R.id.iv_btn_notnow:
			mActivity.goBack();
			break;

		default:
			break;
		}

	}

	private void onNext() {
		SharedPreferencesMgr phoneDb = new SharedPreferencesMgr(mActivity);
		if (phoneDb.getCurDeafSndType(mActivity.m_nProfileModeForRecording) >= 0) {
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(mActivity);
			alertDialog.setTitle(R.string.setup_title);
			alertDialog.setMessage(R.string.setup_ignore_deaf_sound);
			alertDialog.setPositiveButton(R.string.word_yes,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {

							SharedPreferencesMgr phoneDb = new SharedPreferencesMgr(
									mActivity);
							phoneDb.setCurDeafSndType(
									mActivity.m_nProfileModeForRecording, -1);
							for (int type = 0; type < PRJCONST.REC_SOUND_TYPE_CNT; type++) {
								phoneDb.saveDeafSelectedSndIndex(
										mActivity.m_nProfileModeForRecording,
										type, -1);
							}

							if (mActivity.m_nProfileModeForRecording == GlobalValues.m_nProfileIndoorMode) {
								PRJFUNC.initRecordedData(mActivity,
										GlobalValues.m_nProfileIndoorMode);
							}

							mActivity.onNextRecording(true, -1);
						}
					});
			alertDialog.setNegativeButton(R.string.word_no, null);
			alertDialog.show();
		} else {
			mActivity.onNextRecording(true, -1);
		}

	}

	public int getNextRecordingType() {
		int nTypeToRecord = -1;
		for (int idx = 0; idx < m_adapterItems.getCount(); idx++) {

			UIListItemInfo item = m_adapterItems.getItem(idx);
			if (item.m_extraData == null)
				continue;

			if (!item.m_bChecked)
				continue;

			int nSndType = (Integer) item.m_extraData;

			if (!SetupActivity.m_bSoundsRecordable[nSndType])
				continue;

			if (nSndType == PRJCONST.REC_SOUND_TYPE_THEFTALARM) {
				nTypeToRecord = nSndType;
				item.m_bChecked = false;
				break;
			}

			String strRecDir = PRJFUNC.getRecordDir(nSndType,
					mActivity.m_nProfileModeForRecording);

			File recDir = new File(strRecDir);
			if (!recDir.exists() || recDir.list().length == 0) {
				nTypeToRecord = nSndType;
				break;
			}
		}
		return nTypeToRecord;
	}
}
