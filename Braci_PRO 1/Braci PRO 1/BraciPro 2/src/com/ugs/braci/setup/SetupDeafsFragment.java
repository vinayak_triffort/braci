package com.ugs.braci.setup;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;
import android.widget.TextView;

import com.uc.prjcmn.PRJFUNC;
import com.ugs.braci.R;
import com.ugs.braci.adapter.UIListAdapter;
import com.ugs.info.UIListItemInfo;

public class SetupDeafsFragment extends Fragment {

	private SetupActivity mActivity;

	private ListView m_listItems;
	private UIListAdapter m_adapterItems;

	private TextView m_tvDescription;

	private Fragment m_oldFragment;
	private String m_strOldTitle;

	public static final int ITEM_DEAF_BELLMAN = 0;
	public static final int ITEM_DEAF_BYRON = 1;
	public static final int ITEM_DEAF_FIREDLAND = 2;
	public static final int ITEM_DEAF_GREENBROCK = 3;
	public static final int ITEM_DEAF_ECHO = 4;
	public static final int ITEM_DEAF_GEEMARK = 5;
	public static final int ITEM_DEAF_AMPLICOM = 6;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		final View v = inflater.inflate(R.layout.fragment_general, container,
				false);

		mActivity = (SetupActivity) getActivity();

		m_oldFragment = mActivity.m_curFragment;
		m_strOldTitle = mActivity.m_tvTitle.getText().toString();

		mActivity.m_curFragment = SetupDeafsFragment.this;
		mActivity.m_tvTitle.setText(mActivity
				.getString(R.string.setupdeaf_title));
		if (m_oldFragment == null) {
			PRJFUNC.hideFragment(mActivity, mActivity.m_setupFragment);
		} else {
			PRJFUNC.hideFragment(mActivity, m_oldFragment);
		}

		updateLCD(v);

		// - update position
		if (!PRJFUNC.DEFAULT_SCREEN) {

		}

		return v;
	}

	@Override
	public void onDestroyView() {
		if (mActivity.m_curFragment == SetupDeafsFragment.this) {
			mActivity.m_curFragment = m_oldFragment;
			mActivity.m_tvTitle.setText(m_strOldTitle);

			if (mActivity.m_curFragment == null) {
				PRJFUNC.showFragment(mActivity, mActivity.m_setupFragment);
			} else {
				PRJFUNC.showFragment(mActivity, m_oldFragment);
			}
		}

		super.onDestroyView();
	}

	public void refreshList() {
		m_adapterItems.clear();

		UIListItemInfo itemInfo = null;

		itemInfo = new UIListItemInfo(0,
				mActivity.getString(R.string.setupdeaf_bellman_title),
				mActivity.getString(R.string.setupdeaf_bellman_desc), false);
		itemInfo.setChecked(false);
		m_adapterItems.add(itemInfo);

		itemInfo = new UIListItemInfo(0,
				mActivity.getString(R.string.setupdeaf_byron_title),
				mActivity.getString(R.string.setupdeaf_byron_desc), false);
		itemInfo.setChecked(false);
		m_adapterItems.add(itemInfo);

		itemInfo = new UIListItemInfo(0,
				mActivity.getString(R.string.setupdeaf_firedland_title),
				mActivity.getString(R.string.setupdeaf_firedland_desc), false);
		itemInfo.setChecked(false);
		m_adapterItems.add(itemInfo);

		itemInfo = new UIListItemInfo(0,
				mActivity.getString(R.string.setupdeaf_greenbrock_title),
				mActivity.getString(R.string.setupdeaf_greenbrock_desc), false);
		itemInfo.setChecked(false);
		m_adapterItems.add(itemInfo);

		itemInfo = new UIListItemInfo(0,
				mActivity.getString(R.string.setupdeaf_echo_title),
				mActivity.getString(R.string.setupdeaf_echo_desc), false);
		itemInfo.setChecked(false);
		m_adapterItems.add(itemInfo);

		itemInfo = new UIListItemInfo(0,
				mActivity.getString(R.string.setupdeaf_geemark_title),
				mActivity.getString(R.string.setupdeaf_geemark_desc), false);
		itemInfo.setChecked(false);
		m_adapterItems.add(itemInfo);

		itemInfo = new UIListItemInfo(0,
				mActivity.getString(R.string.setupdeaf_amplicom_title),
				mActivity.getString(R.string.setupdeaf_amplicom_desc), false);
		itemInfo.setChecked(false);
		m_adapterItems.add(itemInfo);
	}

	// //////////////////////////////////////////////////
	private void updateLCD(View v) {

		if (PRJFUNC.mGrp == null) {
			PRJFUNC.resetGraphValue(mActivity);
		}

		m_tvDescription = (TextView) v.findViewById(R.id.tv_desc);
		m_tvDescription.setText(mActivity.getString(R.string.setupdeaf_desc));

		m_listItems = (ListView) v.findViewById(R.id.listview);
		m_adapterItems = new UIListAdapter(mActivity, R.layout.list_item_title_and_desc,
				new ArrayList<UIListItemInfo>());
		m_adapterItems.setCallback(m_adapterCallback);
		m_listItems.setAdapter(m_adapterItems);

		refreshList();
	}

	UIListAdapter.Callback m_adapterCallback = new UIListAdapter.Callback() {
		@Override
		public void onItemClick(int p_nPosition) {
			mActivity.onDeafSubProductsSelected(p_nPosition);
		}
	};
}
