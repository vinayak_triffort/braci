/**
 * when create new app this class must update to lastest.
 */

package com.ugs.lib;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

/**
 * 다른 프로젝트들에서도 그대로 리용가능한 안드로이드용 함수들
 * 
 * static 으로 사용가능
 * 
 * @author orient
 */
public class MYSLIB {
	// /////////////////////////////////////////////////////////////////////
	// -- error code
	private static byte ERR_NONE = 0;

	private static byte m_nErrorCode = ERR_NONE;

	// /////////////////////////////////////////////////////////////////////
	// const values
	public static final int TOP = 0x01;
	public static final int LEFT = 0x02;
	public static final int RIGHT = 0x04;
	public static final int BOTTOM = 0x08;
	public static final int HCENTER = 0x10;
	public static final int VCENTER = 0x20;
	public static final int PARSE = 0x40;

	//
	public static final int NO_VALUE = -99999;

	// //////////////////////////////////////////////////////////////////
	// /// 함수들 ///////////////
	// //////////////////////////////////////////////////////////////////

	public byte GetErrorCode() {
		return m_nErrorCode;
	}

	// //////////////////////////////////////////////////////
	// //////////////////////////////////////////////////////
	// //////////////////////////////////////////////////////


	public static String encrypt_sha1(String text)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		MessageDigest md = MessageDigest.getInstance("SHA-1");
		md.update(text.getBytes("iso-8859-1"), 0, text.length());
		byte[] sha1hash = md.digest();
		return convertToHex(sha1hash);
	}

	private static String convertToHex(byte[] data) {
		StringBuilder buf = new StringBuilder();
		for (byte b : data) {
			int halfbyte = (b >>> 4) & 0x0F;
			int two_halfs = 0;
			do {
				buf.append((0 <= halfbyte) && (halfbyte <= 9) ? (char) ('0' + halfbyte)
						: (char) ('a' + (halfbyte - 10)));
				halfbyte = b & 0x0F;
			} while (two_halfs++ < 1);
		}
		return buf.toString();
	}

	/**
	 * 네트워크 상태 정보 체크
	 */
	public static boolean checkNetworkAvailable(Context context) {

		boolean isNetworkAvailable = false;
		ConnectivityManager conn_manager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);

		try {
			NetworkInfo network_info = conn_manager.getActiveNetworkInfo();
			if (network_info != null && network_info.isConnected()) {

				if (network_info.getType() == ConnectivityManager.TYPE_WIFI) {
					// do some staff with WiFi connection
					isNetworkAvailable = true;

				} else if (network_info.getType() == ConnectivityManager.TYPE_MOBILE) {
					// do something with Carrier connection
					isNetworkAvailable = true;

				} else if (network_info.getType() == ConnectivityManager.TYPE_WIMAX) {
					// do something with Wibro/Wimax connection
					isNetworkAvailable = true;
				}
			}
		} catch (Exception e) {
			Log.d("HttpRequester", e.getMessage());
		}

		return isNetworkAvailable;
	}
}
