package com.ugs.lib;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;

/**
 * 컨트�?배치 ?�이브러�?
 * 
 * @author orient
 * @since 2013-03-13
 */
public class LayoutLib {

	public static float X_Z = 1;
	public static float Y_Z = 1;
	public float DENSITY = 1;

	// - constants
	// -- means no change
	public static final int NOVALUE = 99999;
	// -- type of layout params
	public static final int LP_FrameLayout = 101;
	public static final int LP_LinearLayout = 102;
	public static final int LP_TableLayout = 103;
	public static final int LP_RelativeLayout = 104;

	public static final int SCALE_SIZE_WITH_NONE = 0;
	public static final int SCALE_SIZE_WITH_WIDTH = 1;
	public static final int SCALE_SIZE_WITH_HEIGHT = 2;

	public LayoutLib(int dpi1, int dpi2, float x_z, float y_z, float density) {

		float f1 = dpi1 / (dpi2 * 1f);
		X_Z = f1 * x_z;
		Y_Z = f1 * y_z;
		DENSITY = density;
	}

	public void setTextViewFont(Context context, TextView tv, String font) {

		if (context == null || tv == null) {
			return;
		}

		Typeface face = Typeface.createFromAsset(context.getAssets(), font);
		tv.setTypeface(face);

		// tv.setBackgroundColor(m_ctxt.getResources().getColor(R.color.COLOR_DEBUG_GREEN));
	}

	public void setTextViewFont(Context context, TextView tv, String font,
			int style) {

		if (context == null || tv == null) {
			return;
		}

		Typeface face = Typeface.createFromAsset(context.getAssets(), font);
		tv.setTypeface(face, style);
	}

	public void setTextViewFontScale(TextView tv) {

		float size = tv.getTextSize();
		tv.setTextSize(size * X_Z / DENSITY);

	}

	public void setEditTextFontScale(EditText et) {

		float size = et.getTextSize();
		et.setTextSize(size * X_Z / DENSITY);

	}

	public void setButtonFontScale(Button btn) {

		float size = btn.getTextSize();
		btn.setTextSize(size * X_Z / DENSITY);

	}

	public void setTextViewFontScale(TextView tv, float zoom) {

		float size = tv.getTextSize();
		tv.setTextSize(size * zoom);
	}

	public void relayoutTextViewCompoundDrawablePadding(TextView tv, boolean b_x) {
		// int pad = tv.getCompoundDrawablePadding();
		// if (b_x)
		// pad = (int) (pad * X_Z);
		// else
		// pad = (int) (pad * Y_Z);
		// tv.setCompoundDrawablePadding(pad);
	}

	public void relayoutView(View view, int param_type, int scale_type) {

		if (param_type == LP_FrameLayout) {
			relayoutFrameLayoutView(view, scale_type);
		} else if (param_type == LP_LinearLayout) {
			relayoutLinearLayoutView(view, scale_type);
		} else if (param_type == LP_TableLayout) {
			relayoutTableLayoutView(view, scale_type);
		} else if (param_type == LP_RelativeLayout) {
			relayoutRelativeLayoutView(view, scale_type);
		}
	}

	public void relayoutView(View view, int param_type) {

		if (param_type == LP_FrameLayout) {
			relayoutFrameLayoutView(view, SCALE_SIZE_WITH_NONE);
		} else if (param_type == LP_LinearLayout) {
			relayoutLinearLayoutView(view, SCALE_SIZE_WITH_NONE);
		} else if (param_type == LP_TableLayout) {
			relayoutTableLayoutView(view, SCALE_SIZE_WITH_NONE);
		} else if (param_type == LP_RelativeLayout) {
			relayoutRelativeLayoutView(view, SCALE_SIZE_WITH_NONE);
		}
	}

	private void relayoutRelativeLayoutView(View view, int scale_size_type) {

		if (view == null) {
			return;
		}

		RelativeLayout.LayoutParams params = null;
		params = (RelativeLayout.LayoutParams) view.getLayoutParams();

		if (params == null) {
			return;
		}

		if (params.width == LayoutParams.MATCH_PARENT) {
		} else if (params.width == LayoutParams.WRAP_CONTENT) {
		} else {
			params.width = (int) (params.width * X_Z);
		}

		if (params.height == LayoutParams.MATCH_PARENT) {
		} else if (params.height == LayoutParams.WRAP_CONTENT) {
		} else {
			params.height = (int) (params.height * Y_Z);
		}

		if (scale_size_type == SCALE_SIZE_WITH_WIDTH) {
			params.height = params.width;
		} else if (scale_size_type == SCALE_SIZE_WITH_HEIGHT) {
			params.width = params.height;
		}

		params.topMargin = (int) (params.topMargin * Y_Z);
		params.bottomMargin = (int) (params.bottomMargin * Y_Z);
		params.leftMargin = (int) (params.leftMargin * X_Z);
		params.rightMargin = (int) (params.rightMargin * X_Z);

		view.requestLayout();
	}

	private void relayoutFrameLayoutView(View view, int scale_size_type) {

		if (view == null) {
			return;
		}

		FrameLayout.LayoutParams params = null;
		params = (FrameLayout.LayoutParams) view.getLayoutParams();

		if (params == null) {
			return;
		}

		if (params.width == LayoutParams.MATCH_PARENT) {
		} else if (params.width == LayoutParams.WRAP_CONTENT) {
		} else {
			params.width = (int) (params.width * X_Z);
		}

		if (params.height == LayoutParams.MATCH_PARENT) {
		} else if (params.height == LayoutParams.WRAP_CONTENT) {
		} else {
			params.height = (int) (params.height * Y_Z);
		}

		if (scale_size_type == SCALE_SIZE_WITH_WIDTH) {
			params.height = params.width;
		} else if (scale_size_type == SCALE_SIZE_WITH_HEIGHT) {
			params.width = params.height;
		}

		params.topMargin = (int) (params.topMargin * Y_Z);
		params.bottomMargin = (int) (params.bottomMargin * Y_Z);
		params.leftMargin = (int) (params.leftMargin * X_Z);
		params.rightMargin = (int) (params.rightMargin * X_Z);

		view.requestLayout();
	}

	private void relayoutLinearLayoutView(View view, int scale_size_type) {

		if (view == null) {
			return;
		}

		LinearLayout.LayoutParams params = null;
		params = (LinearLayout.LayoutParams) view.getLayoutParams();

		if (params == null) {
			return;
		}

		if (params.width == LayoutParams.MATCH_PARENT) {
		} else if (params.width == LayoutParams.WRAP_CONTENT) {
		} else {
			params.width = (int) (params.width * X_Z);
		}

		if (params.height == LayoutParams.MATCH_PARENT) {
		} else if (params.height == LayoutParams.WRAP_CONTENT) {
		} else {
			params.height = (int) (params.height * Y_Z);
		}

		if (scale_size_type == SCALE_SIZE_WITH_WIDTH) {
			params.height = params.width;
		} else if (scale_size_type == SCALE_SIZE_WITH_HEIGHT) {
			params.width = params.height;
		}

		params.topMargin = (int) (params.topMargin * Y_Z);
		params.bottomMargin = (int) (params.bottomMargin * Y_Z);
		params.leftMargin = (int) (params.leftMargin * X_Z);
		params.rightMargin = (int) (params.rightMargin * X_Z);

		view.requestLayout();
	}

	private void relayoutTableLayoutView(View view, int scale_size_type) {

		TableLayout.LayoutParams params = null;
		params = (TableLayout.LayoutParams) view.getLayoutParams();

		if (params == null) {
			return;
		}

		if (params.width == LayoutParams.MATCH_PARENT) {
		} else if (params.width == LayoutParams.WRAP_CONTENT) {
		} else {
			params.width = (int) (params.width * X_Z);
		}

		if (params.height == LayoutParams.MATCH_PARENT) {
		} else if (params.height == LayoutParams.WRAP_CONTENT) {
		} else {
			params.height = (int) (params.height * Y_Z);
		}

		if (scale_size_type == SCALE_SIZE_WITH_WIDTH) {
			params.height = params.width;
		} else if (scale_size_type == SCALE_SIZE_WITH_HEIGHT) {
			params.width = params.height;
		}

		params.topMargin = (int) (params.topMargin * Y_Z);
		params.bottomMargin = (int) (params.bottomMargin * Y_Z);
		params.leftMargin = (int) (params.leftMargin * X_Z);
		params.rightMargin = (int) (params.rightMargin * X_Z);

		view.requestLayout();
	}

	/**
	 * view control is repositioned with new parameters.
	 * 
	 * @param view
	 * @param param_type
	 *            : Type of LayoutParams
	 * @param width
	 *            : width of Layout Parameters
	 * @param height
	 *            : height of Layout Parameters
	 * @param m_left
	 *            : Margins-left of Layout Parameters
	 * @param m_top
	 *            : Margins-top of Layout Parameters
	 * @param m_right
	 *            : Margins-right of Layout Parameters
	 * @param m_bottom
	 *            : Margins-bottom of Layout Parameters
	 * @param pad_left
	 *            : Padding-left of View
	 * @param pad_top
	 *            : Padding-top of View
	 * @param pad_right
	 *            : Padding-right of View
	 * @param pad_bottom
	 *            : Padding-bottom of View
	 */
	@SuppressWarnings("deprecation")
	public void relayoutView(View view, //
			int param_type, //
			int width, int height, //
			int m_left, int m_top, int m_right, int m_bottom) {

		LayoutParams params = null;

		if (param_type == LP_LinearLayout) {
			params = (LinearLayout.LayoutParams) view.getLayoutParams();
		}

		if (params == null) {
			return;
		}

		if (width != NOVALUE)
			if (width == LayoutParams.MATCH_PARENT) {
				params.width = width;
			} else if (width == LayoutParams.WRAP_CONTENT) {
				params.width = width;
			} else if (width == LayoutParams.FILL_PARENT) {
				params.width = width;
			} else {
				params.width = (int) (X_Z * width);
			}
		if (height != NOVALUE) {
			if (height == LayoutParams.MATCH_PARENT) {
				params.height = height;
			} else if (height == LayoutParams.WRAP_CONTENT) {
				params.height = height;
			} else if (height == LayoutParams.FILL_PARENT) {
				params.height = height;
			} else {
				params.height = (int) (Y_Z * height);
			}
		}
		if (m_left != NOVALUE) {
			params.leftMargin = (int) (X_Z * m_left);
		}
		if (m_top != NOVALUE) {
			params.topMargin = (int) (Y_Z * m_top);
		}
		if (m_right != NOVALUE) {
			params.rightMargin = (int) (X_Z * m_right);
		}
		if (m_bottom != NOVALUE) {
			params.bottomMargin = (int) (Y_Z * m_bottom);
		}
	}

	/**
	 * change padding position of view. (pixel)
	 * 
	 * @param view
	 * @param left
	 * @param top
	 * @param right
	 * @param bottom
	 */
	public void repaddingView(View view) {

		if (view == null) {
			return;
		}

		int left, top, right, bottom;

		left = view.getPaddingLeft();
		top = view.getPaddingTop();
		right = view.getPaddingRight();
		bottom = view.getPaddingBottom();

		left = (int) (X_Z * left);
		right = (int) (X_Z * right);
		top = (int) (Y_Z * top);
		bottom = (int) (Y_Z * bottom);

		view.setPadding(left, top, right, bottom);
	}

	/**
	 * change padding position of view. (pixel)
	 * 
	 * @param view
	 * @param left
	 * @param top
	 * @param right
	 * @param bottom
	 */
	public void repaddingView(View view, int left, int top, int right,
			int bottom) {

		if (left == NOVALUE)
			left = view.getPaddingLeft();
		else
			left = (int) (X_Z * left);

		if (top == NOVALUE)
			top = view.getPaddingTop();
		else
			top = (int) (Y_Z * top);

		if (right == NOVALUE)
			right = view.getPaddingRight();
		else
			right = (int) (X_Z * right);

		if (bottom == NOVALUE)
			bottom = view.getPaddingBottom();
		else
			bottom = (int) (Y_Z * bottom);

		view.setPadding(left, top, right, bottom);
	}

	/**
	 * view control is repositioned with new parameters.
	 * 
	 * @param view
	 * @param param_type
	 *            : Type of LayoutParams
	 * @param width
	 *            : width of Layout Parameters
	 * @param height
	 *            : height of Layout Parameters
	 * @param m_left
	 *            : Margins-left of Layout Parameters
	 * @param m_top
	 *            : Margins-top of Layout Parameters
	 * @param m_right
	 *            : Margins-right of Layout Parameters
	 * @param m_bottom
	 *            : Margins-bottom of Layout Parameters
	 * @param pad_left
	 *            : Padding-left of View
	 * @param pad_top
	 *            : Padding-top of View
	 * @param pad_right
	 *            : Padding-right of View
	 * @param pad_bottom
	 *            : Padding-bottom of View
	 */
	public void relayoutViewEx(View view, //
			int param_type, //
			int width, int height, //
			int m_left, int m_top, int m_right, int m_bottom, //
			int pad_left, int pad_top, int pad_right, int pad_bottom) {

		relayoutView(view, param_type, width, height, m_left, m_top, m_right,
				m_bottom);

		repaddingView(view, pad_left, pad_top, pad_right, pad_bottom);
	}
	
	public double getXRate() {
		return X_Z * DENSITY / 2;
	}
	
	public double getYRate() {
		return Y_Z * DENSITY / 2;
	}
}
