package com.ugs.scrollmenu;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

public class MyScrollView extends ScrollView {

	private Runnable scrollerTask;
	private int initialPosition;

	private int newCheck = 100;
	private static final String TAG = "MyScrollView";

	public interface OnScrollStoppedListener {
		void onScrollStopped();
	}
	
	private boolean m_bRunning = false;

	private OnScrollStoppedListener onScrollStoppedListener;

	public MyScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);

		scrollerTask = new Runnable() {

			public void run() {	

				int newPosition = getScrollY();
				if (initialPosition - newPosition == 0) {// has stopped

					if (onScrollStoppedListener != null) {

						onScrollStoppedListener.onScrollStopped();
					}
					m_bRunning = false;
				} else {
					initialPosition = getScrollY();
					MyScrollView.this.postDelayed(scrollerTask, newCheck);
				}
			}
		};
	}

	public void setOnScrollStoppedListener(
			MyScrollView.OnScrollStoppedListener listener) {
		onScrollStoppedListener = listener;
	}

	public void startScrollerTask() {
		if (m_bRunning)
			return;
		
		initialPosition = getScrollY();
		MyScrollView.this.postDelayed(scrollerTask, newCheck);
		m_bRunning = true;
	}

}
